import { SVGProps } from "react";

const FacebookLogo = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={20}
    height={20}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M18.333 10a8.334 8.334 0 0 0-16.666 0A8.334 8.334 0 0 0 10 18.333c.049 0 .098 0 .146-.002v-6.487h-1.79V9.758h1.79V8.222c0-1.78 1.087-2.75 2.676-2.75.76 0 1.414.057 1.605.083v1.86h-1.095c-.864 0-1.032.411-1.032 1.014v1.329h2.066l-.269 2.086H12.3v6.168A8.338 8.338 0 0 0 18.333 10Z"
      fill="#fff"
    />
  </svg>
);

export default FacebookLogo;
