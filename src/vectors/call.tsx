import * as React from "react"
import { SVGProps } from "react"

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={30}
    height={31}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <rect
      x={0.199}
      y={0.601}
      width={29.602}
      height={29.602}
      rx={3.524}
      fill="#244D08"
      fillOpacity={0.4}
    />
    <path
      d="M8.206 7.65a.964.964 0 0 0-.959.97v1.938c0 6.957 5.581 12.598 12.466 12.598h1.918c.53 0 .958-.434.958-.97V20.1a.966.966 0 0 0-.8-.956l-4.254-.716a.956.956 0 0 0-1.016.523l-.742 1.5a10.64 10.64 0 0 1-5.853-5.916l1.484-.75a.972.972 0 0 0 .517-1.026l-.709-4.299a.962.962 0 0 0-.946-.81H8.206Z"
      fill="#1D2319"
    />
    <path
      d="M15.408 8.475c-.901 0-1.632.604-1.632 1.35v2.699c0 .745.73 1.35 1.632 1.35h1.632v2.024l2.448-2.024h1.632c.902 0 1.633-.605 1.633-1.35v-2.7c0-.745-.731-1.349-1.633-1.349h-5.712Z"
      fill="#1D2319"
    />
  </svg>
)

export default SvgComponent
