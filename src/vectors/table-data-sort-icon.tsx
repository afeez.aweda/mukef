import * as React from "react";
import { SVGProps } from "react";

const TableDataSortIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={17}
    height={16}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11.36 9.527 8.5 12.393 5.64 9.527a.67.67 0 1 0-.947.946l3.333 3.334a.666.666 0 0 0 .947 0l3.333-3.334a.672.672 0 0 0 0-.946.67.67 0 0 0-.946 0ZM5.64 6.473 8.5 3.607l2.86 2.866a.667.667 0 0 0 1.092-.217.667.667 0 0 0-.146-.73L8.973 2.194a.667.667 0 0 0-.947 0L4.693 5.527a.67.67 0 0 0 .947.946Z"
      fill="#434343"
    />
  </svg>
);

export default TableDataSortIcon;
