import * as React from "react"
import { SVGProps } from "react"

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={36}
    height={43}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.626 5.126c6.834-6.835 17.914-6.835 24.748 0 6.835 6.834 6.835 17.914 0 24.748L18 42.25 5.626 29.874c-6.835-6.834-6.835-17.914 0-24.748ZM18 22.5a5 5 0 1 0 0-10 5 5 0 0 0 0 10Z"
      fill="#1D2319"
    />
  </svg>
)

export default SvgComponent
