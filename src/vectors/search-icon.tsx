import { SVGProps } from "react";

const SearchIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    width={17}
    height={16}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M14.973 13.526 12.5 11.073a6 6 0 1 0-.927.926l2.454 2.454a.665.665 0 0 0 .946 0 .667.667 0 0 0 0-.927ZM7.833 12a4.666 4.666 0 1 1 0-9.332 4.666 4.666 0 0 1 0 9.332Z"
      fill="#757886"
    />
  </svg>
);

export default SearchIcon;
