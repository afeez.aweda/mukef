import * as React from "react"
import { SVGProps } from "react"

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={30}
    height={30}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <rect
      width={29.602}
      height={29.602}
      rx={3.524}
      fill="#244D08"
      fillOpacity={0.4}
    />
    <path
      d="m6.347 10.448 8.454 4.227 8.454-4.227a2.115 2.115 0 0 0-2.111-1.991H8.458c-1.127 0-2.047.88-2.111 1.991Z"
      fill="#1D2319"
    />
    <path
      d="m23.258 12.81-8.457 4.23-8.458-4.23v6.219c0 1.168.947 2.114 2.115 2.114h12.686a2.114 2.114 0 0 0 2.114-2.114V12.81Z"
      fill="#1D2319"
    />
  </svg>
)

export default SvgComponent
