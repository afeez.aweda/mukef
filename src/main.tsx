import React from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "styled-components";
import appTheme from "./styles/appTheme";
import GlobalStyles from "./styles/globalStyles";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { AppContextProvider } from "./context/context";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <GlobalStyles />
    <ThemeProvider theme={appTheme}>
      <AppContextProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </AppContextProvider>
    </ThemeProvider>
  </React.StrictMode>
);
