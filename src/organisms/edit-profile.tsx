import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import Button from "../atoms/button";
import Checkbox from "../atoms/checkbox";
import Input from "../atoms/input";
import { Text } from "../atoms/Typography";
import { AppContext } from "../context/context";
import SelectMenu from "../molecules/select-menu";
import CloseIcon from "../vectors/close-icon";

const EditProfileContainer = styled.div`
  height: 38rem;
  overflow: auto;
  ::-webkit-scrollbar {
    display: none;
  }
  & {
    -ms-overflow-style: none;
    scrollbar-width: none;
  }
  .heading {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1.5rem;
  }
  .input {
    display: flex;
    flex-direction: column;
    row-gap: 1.5rem;
    .title {
      display: flex;
      align-items: center;
      column-gap: 1.03rem;
    }
    .phone-number,
    .state,
    .change-password .password,
    .btn {
      display: flex;
      column-gap: 0.98rem;
    }
    .btn {
      align-items: center;
      justify-content: space-between;
    }
    .change-password {
      label {
        margin-bottom: 1.875rem;
        text-transform: capitalize;
        font-size: 0.8775rem;
        line-height: 1.42rem;
        font-family: Poppins;
        color: ${({ theme }) => theme.colors["inputBg2"]};
      }
    }
  }
  @media screen and (max-width: 500px) {
    height: 24rem;
    width: 20rem;
    .heading {
      margin-bottom: 0.8rem;
    }
    .input {
      row-gap: 1rem;
      .state {
        align-items: center;
      }
    }
    .btn {
      flex-direction: column;
      row-gap: 0.8rem;
    }
  }
`;

const EditProfile = () => {
  const { dispatch } = useContext(AppContext);

  const navigate = useNavigate();
  const handleModal = () => {
    dispatch({ type: "FALSE" });
  };
  const handleNavigate = () => {
    dispatch({ type: "TOGGLE" });
    navigate("/dashboard/profile");
  };

  const titles = ["Prof", "Sheikh", "Alhaji", "Alhaja"];
  const states = ["Kwara", "Abuja", "Lagos", "Sokoto", "Bornu"];
  return (
    <EditProfileContainer>
      <div className="heading">
        <Text fs="1.83rem" lh="2.6rem" color="black">
          Edit Profile
        </Text>{" "}
        <CloseIcon onClick={handleModal} />
      </div>
      <div className="input">
        <div className="title">
          <SelectMenu defaultValue="Prof" options={titles} label="Title" />
          <Input placeholder="Abdul-Ganiyy Oladosu AGAS..." label="Full name" />
        </div>
        <Input placeholder="profagas@gmail.com" label="Email" />
        <div className="phone-number">
          <Input label="Phone number" placeholder="080123456789" />
          <Input label="Phone number" placeholder="080123456789" />
        </div>
        <Input label="Address" placeholder="profagas@gmail.com" />
        <div className="state">
          <SelectMenu
            defaultValue="Kwara"
            options={states}
            label="State"
            wt="100%"
          />
          <Input label="Nationality" placeholder="Nigerian" />
        </div>
        <Input
          label="Favourite quote"
          placeholder="إِنَّ سَعْيَكُمْ لَشَتَّىٰ"
        />
        <div className="change-password">
          <label htmlFor="change-password">Change Password</label>
          <div className="password">
            <Input placeholder="Password" />{" "}
            <Input placeholder="Confirm Password" />
          </div>
        </div>
        <div className="btn">
          <Checkbox text="Receive update from us" />
          <Button title="Save" />
        </div>
      </div>
    </EditProfileContainer>
  );
};

export default EditProfile;
