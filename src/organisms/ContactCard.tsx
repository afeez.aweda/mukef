import styled from 'styled-components'
import { Text } from '../atoms/Typography'
import Email from '../vectors/email'
import Call from '../vectors/call'
import Support from '../vectors/support'
import Button from '../atoms/button'
import Location from '../vectors/location'
import Arrow from '../vectors/arrow'
import Input from '../atoms/input'
import Maps from '../vectors/images/map.png'

const Wrapper = styled.div`
  padding: 22px 47px;
  width: 58%;
  border-radius: 15px;
  background-color: #fff;
  box-shadow: 0px -4px 15px rgba(255, 255, 255, 0.39), 0px 4px 11px rgba(29, 35, 25, 0.35);
  display: flex;
  position: absolute;
  top: -17rem;

  @media screen and (max-width: 1000px){
    width: 85%;
  }

  @media screen and (max-width: 700px){
    flex-direction: column-reverse;
  }
`
const Left = styled.div`
  width: 20.44rem;
  padding-top: 38px;
  padding-bottom: 68px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-right: 1.37px solid #A9A9A9;

  @media screen and (max-width: 700px){
    border-right: none;
    border-top: 1.37px solid #A9A9A9;
    width: 100%;
  }
`
const Right = styled.div`
  margin: 32px 0 28px 37px;

  textarea{
   resize: none;
   padding: 1rem;
   border-radius: 5px;
   font-size: 16px;
   outline: none;
  }

  @media screen and (max-width: 700px){
    margin: 0 0 28px;
  }
`
const LeftTop = styled.div` 
  max-width: 13.5rem;
`
const Contact = styled.div`
  margin-bottom: 2.5rem;
  @media screen and (max-width: 400px){
    .ext{
      font-size: 14px;
    }
  }
`
const Map = styled.div`
  background-image: url(${Maps});
  background-repeat: no-repeat;
  background-size: cover;
  height: 17rem;
  border-radius: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5.75rem 0 2.7rem;
`
const MapItems = styled.div`
  display: flex;
`
const Dot = styled.div`
  width: 9px;
  height: 9px;
  border-radius: 50%;
  background-color: #E62020;
  margin-right: 4.78px;
`
const Black = styled.div`
  width: 5.2rem;
  padding: 5px 0 5px 7.5px;
  background-color: #1C1C1C;
`
const Bottom = styled.div`
  display: flex;
  margin-left: 1rem;

  @media screen and (max-width: 400px){
    margin: 0;
  }
`
const BottomText = styled.div`
  margin-left: 18.5px;
  width: 80%;

  @media screen and (max-width: 400px){
    margin-left: 0;
    .ext{
      font-size: 15px;
    }
  }
`
const Inputs = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
  gap: 1rem;

  @media screen and (max-width: 400px){
    flex-direction: column;
  }
`
const inputArray = [
  {placeholder: 'Your email'},
  {placeholder: 'Your name'},
]

const ContactCard = () => {
  return (
    <Wrapper>
      <Left>
        <LeftTop>
          <Contact>
            <Email/>
            <Text className='ext' fs='13px' fw={700}>Email us</Text>
            <Text className='ext' fs='13px' fw={600}>info@mufek.com.ng</Text>
            <Text className='ext' fs='13px' >lorem ispum lorem ispumlorem ispum lorem ispumdsaddsdddd</Text>
          </Contact>
          <Contact>
            <Call/>
            <Text className='ext' fs='13px' fw={700}>Call us</Text>
            <Text className='ext' fs='13px' fw={600}>+234 8038 090 0938</Text>
            <Text className='ext' fs='13px' >lorem ispum lorem ispumlorem ispum wwwedddd ispum</Text>
          </Contact>
          <Contact>
            <Support/>
            <Text className='ext' fs='13px' fw={700}>Support</Text>
            <Text className='ext' fs='13px'>lorem ispum lorem ispumlorem ispum lorem ispewwwum</Text>
            <div style={{display: 'flex'}}>
            <Text className='ext' fs='13px' fw={600}>Support center </Text>
            <div style={{width:'1.3rem', marginTop:'0.2rem'}}><Arrow/></div>
            </div>
          </Contact>
        </LeftTop>
      </Left>
      <Right>
        <Inputs>
          {inputArray.map(({placeholder}) => <Input key={placeholder} bg='transparent' pd='1rem' border='1px solid #A29E9E' placeholder={placeholder}/>)}
        </Inputs>
        <textarea rows={6} style={{width: '100%'}} placeholder='Your message' />
        <Button wt='10.56rem'  mg='30px 0 0' title='Send'/>
        <Map>
          <MapItems>
            <Dot/>
            <Black>
            <p style={{color: '#C3C3C3', fontSize: '7px', lineHeight: '5px'}}>We are here!</p>
            <p style={{color: '#fff', fontSize: '8px', lineHeight: '10px'}}>Muhammad Kamalud-deen University Ilorin Kwara State</p>
            </Black>
          </MapItems>
        </Map>
        <Bottom>
            <Location/>
          <BottomText>
            <Text className='ext' fw={600} mg='0 0 15px' fs='20px'>Muhammad Kamaldeen University Ilorin Kwara snate</Text>
            <Text className='ext' fw={500} fs='16px'> - Hi. My name is Khoa. I am a </Text>
          </BottomText>
        </Bottom>
      </Right>
    </Wrapper>
  )
}

export default ContactCard