import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import Button from "../atoms/button";
import Checkbox from "../atoms/checkbox";
import Input from "../atoms/input";
import { Text } from "../atoms/Typography";
import { AppContext } from "../context/context";
import SelectMenu from "../molecules/select-menu";
import CloseIcon from "../vectors/close-icon";

const LoginContainer = styled.div`
  margin: 1.5rem 2.5rem 2.5rem 2.5rem;
  .heading {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 3.3rem;
  }
  .input {
    display: flex;
    flex-direction: column;
    row-gap: 2rem;
  }
  .check {
    display: flex;
    column-gap: 3.19rem;
    margin: 2rem 0;
  }
  .or {
    display: flex;
    align-items: center;
    margin: 2rem 0 2rem 0;
    p {
      background-color: ${({ theme }) => theme.colors["inputBg2"]};
      flex: 1;
      height: 0.1rem;
    }
    span {
      margin: 0 0.5rem;
      font-size: 1.125rem;
      line-height: 1.75rem;
      color: ${({ theme }) => theme.colors["inputBg2"]};
    }
  }
  .alt-opt {
    display: flex;
    justify-content: space-between;
  }
  @media screen and (max-width: 900px) {
    .heading {
      margin-bottom: 1rem;
    }
    .check {
      width: 20rem;
      column-gap: 1rem;
      .forget-pswd {
        font-size: 1rem;
      }
    }
    .or {
      margin: 0.5rem 0;
    }
    .alt-opt {
      .alt-1,
      .alt-2 {
        font-size: 1rem;
      }
    }
  }

  @media screen and (max-width: 500px) {
    margin: 1.5rem;
    .heading {
      margin-bottom: 1rem;
    }
    .check {
      width: 19rem;
      justify-content: space-between;
      .forget-pswd {
        font-size: 0.87rem;
      }
    }
    .or {
      margin: 0.5rem 0;
      font-size: 0.87rem;
    }
    .alt-opt {
      .alt-1,
      .alt-2 {
        font-size: 0.87rem;
      }
    }
  }
`;

const LoginScreen = () => {
  const { dispatch } = useContext(AppContext);

  const navigate = useNavigate();
  const handleModal = () => {
    dispatch({ type: "FALSE" });
  };
  const handleNavigate = () => {
    dispatch({ type: "TOGGLE" });
    navigate("/dashboard/profile");
  };
  const navigateHandler = () => {
    dispatch({ type: "TOGGLE" });
    navigate("/subscribe");
  };
  return (
    <LoginContainer>
      <div className="heading">
        <Text fs="1.83rem" lh="2.6rem" color="black">
          Login
        </Text>{" "}
        <CloseIcon onClick={handleModal} />
      </div>
      <div className="input">
        <Input placeholder="Email or phonenumber" />
        <Input placeholder="Password" type="password" />
      </div>
      <div className="check">
        <Checkbox text="Remember me" />
        <Text color="black" className="forget-pswd">
          Forget password ?
        </Text>
      </div>
      <Button title="Login" wt="100%" onClick={handleNavigate} />
      <div className="or">
        <p></p>
        <span>or</span>
        <p></p>
      </div>
      <div className="alt-opt">
        <Text fs="1.125rem" lh="1.75rem" color="inputBg2" className="alt-1">
          Don't have account ?
        </Text>
        <Text
          fs="1.2175rem"
          fw={500}
          lh="1.83rem"
          color="primary"
          onClick={navigateHandler}
          className="alt-2"
        >
          Subscribe now
        </Text>
      </div>
    </LoginContainer>
  );
};

export default LoginScreen;
