import styled from "styled-components";
import { Heading } from "../atoms/Typography";
import FacebookLogo from "../vectors/facebook-white-logo";
import FooterLogo from "../vectors/footer-logo";
import InstagramLogo from "../vectors/instagram-white-logo";
import TwitterLogo from "../vectors/twitter-logo";

const FooterWrapper = styled.div`
  width: 100%;
  background: ${({ theme }) => theme.colors["secondary"]};
  padding-top: 4.4rem;
  .footer-navs {
    display: grid;
    grid-template-columns: 0.5fr auto;
    border-bottom: ${({ theme }) => `1px solid ${theme.colors["light"]}`};
    padding-bottom: 1.875rem;
    .footer-links {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
    }
  }
  .footer-cr {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0.9375rem 0;
    .logos {
      display: flex;
      column-gap: 0.625rem;
      .logo {
        width: 3.125rem;
        height: 3.125rem;
        border-radius: 50%;
        background: ${({ theme }) => theme.colors["light"]};
        display: grid;
        place-items: center;
      }
    }
  }
  @media screen and (max-width: 1137px) {
    .footer-navs {
      display: flex;
      flex-direction: column;
      .footer-logo {
        display: flex;
        justify-content: center;
        margin-bottom: 2rem;
      }
      .footer-links {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: flex-start;
        gap: 2rem;
      }
    }
  }
  @media screen and (max-width: 780px) {
    .footer-navs {
      display: flex;
      flex-direction: column;
      .footer-logo {
        display: flex;
        justify-content: center;
        margin-bottom: 2rem;
      }
    }
  }
`;

const Footer = () => {
  const footerLinks = [
    ["Events", "Become Affiliate", "Go Unlimited", "Services"],
    ["News", "Business Stories", "Digital Store", "Learning", "Social Media"],
    [
      "Completed Projects",
      "Design Systems",
      "Theme & Templates",
      "Mockups",
      "Presentations",
      "Wirerames Kits",
      "UI Kits",
    ],
    ["Ongoing Projects", "License", "Refund Policy", "About Us", "Contacts"],
  ];
  const footerLogo = [<FacebookLogo />, <InstagramLogo />, <TwitterLogo />];
  return (
    <FooterWrapper>
      <div className="center">
        <div className="footer-navs">
          <div className="footer-logo">
            <FooterLogo />
          </div>
          <div className="footer-links">
            {footerLinks.map((arrLinks, i) => (
              <div key={`arrLinks-${i}`} className="footer-link">
                {arrLinks.map((navLink, i) => (
                  <Heading
                    fw={500}
                    fs={i === 0 ? "1.25rem" : "1.125rem"}
                    color={i === 0 ? "active" : "light2"}
                    pd={i === 0 ? "0 0 1.25rem 0" : "0 0 1rem 0"}
                    key={navLink}
                  >
                    {navLink}
                  </Heading>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div className="footer-cr">
          <Heading fs="1.125rem" color="light2">
            Copyright © 2020
          </Heading>
          <div className="logos">
            {footerLogo.map((logo, i) => (
              <div className="logo" key={`footer-logo-${i}`}>
                {logo}
              </div>
            ))}
          </div>
        </div>
      </div>
    </FooterWrapper>
  );
};

export default Footer;
