import styled from "styled-components";
import NavLogo from "../vectors/nav-logo";
import { Heading, Text } from "../atoms/Typography";
import Button from "../atoms/button";
import { Link, useLocation, useParams } from "react-router-dom";
import { useContext, useState } from "react";
import { AppContext } from "../context/context";
import MobileIcon from "../vectors/mobile-icon";
import CloseNavIcon from "../vectors/close-nav-icon";

const NavWrapper = styled.div<{ show?: boolean }>`
  width: 100%;
  padding: 2rem 0;
  background: ${({ theme }) => theme.colors["secondary"]};

  .nav,
  .logo,
  .links,
  .link {
    display: flex;
    justify-content: space-between;

    .logo {
      justify-content: center;
      align-items: center;
      column-gap: 2rem;
    }
    .links {
    }
    .links,
    .link {
      column-gap: 7rem;
      align-items: center;
      .link {
        column-gap: 1.6875rem;
      }
    }
    .mobile-icon {
      display: none;
    }
    @media screen and (max-width: 980px) {
      .nav {
        position: relative;
      }
      .links {
        position: absolute;
        height: 38vh;
        right: ${({ show }) => (show ? "calc(6vw - 0.5rem)" : "-1000rem")};
        flex-direction: column;
        align-items: flex-start;
        row-gap: 0.5rem;
        top: calc(10vh - 0.2rem);
        background: ${({ theme }) => theme.colors.secondary};
        padding: 1rem 2rem;
        z-index: 10;
        margin-top: 0.5rem;
      }
      .link {
        display: grid;
        row-gap: 1rem;
      }
      .mobile-icon {
        display: flex;
        align-items: center;
        padding-right: 0.5rem;
      }
    }
  }
`;

const NavBar = () => {
  const { dispatch } = useContext(AppContext);
  const { pathname } = useLocation();
  const handleModal = () => {
    dispatch({ type: "TOGGLE", modal: "login" });
  };
  const [showNavBar, setShowNavBar] = useState(false);

  const handleMobileNavDisplay = () => setShowNavBar(!showNavBar);
  const links = [
    { path: "Home", to: "/" },
    { path: "News", to: "/news" },
    { path: "Projects", to: "/projects" },
    { path: "About Us", to: "/about-us" },
    { path: "Contact", to: "/contact" },
  ];

  return (
    <NavWrapper show={showNavBar}>
      <div className="nav center">
        <div className="logo">
          <NavLogo />
          <Heading color="active" fs="2.125rem" fw={500}>
            MUKEF
          </Heading>
        </div>
        <div className="links">
          <div className="link">
            {links.map(({ path, to }) => (
              <Link to={to} key={path}>
                <Text color={to === pathname ? "light2" : "active"}>
                  {path}
                </Text>
              </Link>
            ))}
          </div>
          {pathname.includes("dashboard") ? (
            <Button outline title="Log Out" color="primary" wt="8rem" />
          ) : (
            <Button
              outline
              title="Login"
              color="primary"
              wt="8rem"
              onClick={handleModal}
            />
          )}
        </div>
        <div className="mobile-icon">
          {showNavBar ? (
            <CloseNavIcon onClick={handleMobileNavDisplay} />
          ) : (
            <MobileIcon onClick={handleMobileNavDisplay} />
          )}
        </div>
      </div>
    </NavWrapper>
  );
};

export default NavBar;
