import styled from 'styled-components'
import Button from '../atoms/button'
import Image from '../atoms/image'
import Input from '../atoms/input'
import { Text } from '../atoms/Typography'
import Images from '../vectors/images/image01.png'
import CloseIcon from "../vectors/close-icon";
import { useContext } from 'react'
import { AppContext } from '../context/context'

const Wrapper = styled.div`
  border-radius: 15px;
  .icon{
    display: flex;
    justify-content: flex-end;
    margin-right: 12px;
  }
  @media screen and (max-width: 600px) {
    width: 18rem;
  }
`
const Main = styled.div`
  height: 36rem;
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 0px 2rem 2rem;

  @media screen and (max-width: 950px) {
    display: flex;
    flex-direction: column;
    overflow: auto;
    ::-webkit-scrollbar {
    display: none;
    }
  }
`
const Left = styled.div`
  padding-right: 2rem;
  border-right: 1px solid lightgray;
  @media screen and (max-width: 950px) {
    border-right: none;
    border-bottom: 1px solid lightgray;
    padding-right: 0;
    margin-bottom: 2rem;
    padding-bottom: 2rem;
  }
  @media screen and (max-width: 600px) {
    .mainText{font-size: 14px}
    .title{margin: 1.3rem 0px 1rem}
  }
`
const Right = styled.div`
  margin-left: 2rem;
  .btn{
    display: flex;
    justify-content: center;
  }
  @media screen and (max-width: 950px) {
    margin-left: 0;
  }
`
const Inputs = styled.div`
  margin: 1.2rem 0;
  display: flex;
  row-gap: 1rem;
  flex-direction: column;
  border-bottom: 1px solid lightgray;
  padding-bottom: 1.5rem;
`
const Select = styled.select<{indx?:number}>`
  background-color: #B6B6B6;
  border: none;
  outline: none;
  border-radius: 4.5px;
  width: 80%;
  padding: 13px;
  font-size: 1rem;
  border-right: 12px solid transparent;
`
const CVV = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: 2rem;
  width: 100%;
`
const Info = styled.div`
  width: 2.4rem;
  height: 2rem;
  border-radius: 50%;
  background-color: #B6B6B6;
  color: #fff;
  font-size: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Year = styled.div`
  width: 100%;
  padding: 9px;
  border-radius: 5px;
  background-color: #B6B6B6;
  color: #858585;
  display: flex;
  align-items: center;
`
const Slash = styled.div`
  width: 12%;
  border: 0.8px solid gray;
  transform: rotate(-60deg) translateY(100%);
`
const MiniInput = styled.input`
  background: #D0D0D0;
  border: 1px solid #858585;
  border-radius: 5px;
  height: 1.8rem;
  width: 24%;
  outline: none;
  text-align: center;
`
const Save = styled.div`
  display: flex;
  margin-bottom: 1rem;

  input[type='checkbox']{
  width: 18px !important;
  height: 18px !important;
  margin: 4px;
  appearance: none;
  border-radius: 3px;
  text-align: center;
  background: #B6B6B6;
  }

  input[type='checkbox']:checked:after {
    content: '✔';
   color: white;
  }
  @media screen and (max-width: 600px) {
    .ext{font-size: 14px}
  }
`
const titleArray = [
  {title : 'Payment method'},
  {title : 'Mastercard'},
  {title : 'Visa'},
  {title : 'PayPal'},
]
const SupportProject = () => {
  const { dispatch } = useContext(AppContext);
  const handleModal = () => {
    dispatch({ type: "FALSE" });
  };
  return (
    <Wrapper>
      <div className='icon'>
        <CloseIcon onClick={handleModal} />
      </div>
      <Main>
      <Left>
        <Text fw={500} fs='1.3rem'>
          Support this project
        </Text>
        <Text className='title' mg='3.2rem 0 1.2rem' fs='18px'>
          Project title goes here 
        </Text>
        <div style={{marginBottom: '1.7rem'}}>
        <Image imgUrl={Images} />
        </div>
        <Text className='mainText' fs='14px'>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature Contrary to popular belief, Lorem Ipsum is not simply random .</Text>
      </Left>
      <Right>
        <Text>Payment information</Text>
        <Inputs>
          <Input pd='10px' placeholder='Amount'/>
          <Select name="" id="">
          {titleArray.map(({title}, index) => 
          <option key={title} style={{color: '#858585'}} disabled={index  === 0} value="">{title}</option>
          )}
          </Select>
          <Input pd='10px' placeholder='Cardholder'/>
          <Input pd='10px' placeholder='Card number'/>
          <CVV>
          <Input pd='10px' placeholder='CVV'/>
          <Info>?</Info>
          </CVV>
          <Year> <Text color='#858585' mg='0 10px 0 4px'>Expiry</Text>  <MiniInput placeholder='M'/> <Slash/> <MiniInput placeholder='Y'/></Year>
        </Inputs>
        <Save>
          <input type='checkbox'/>
          <Text className='ext' fs='14.56px' mg='0 0 0 13.6px'>Save my details for donations</Text>
        </Save>
        <div className='btn'>
          <Button wt='80%' title='Add Donation'/>
        </div>
      </Right>
      </Main>
    </Wrapper>
  )
}

export default SupportProject