import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import Button from "../atoms/button";
import Checkbox from "../atoms/checkbox";
import Input from "../atoms/input";
import { Text } from "../atoms/Typography";
import { AppContext } from "../context/context";
import SelectMenu from "../molecules/select-menu";
import CloseIcon from "../vectors/close-icon";

const LoginContainer = styled.div`
  margin: 1.5rem 2.5rem 2.5rem 2.5rem;
  .heading {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
  }
  .input {
    display: flex;
    flex-direction: column;
    row-gap: 1.5rem;
    .title {
      display: flex;
      align-items: center;
      column-gap: 1.03rem;
    }
  }
  .check {
    display: flex;
    justify-content: space-between;
    margin: 1rem 0;
  }
  .or {
    display: flex;
    align-items: center;
    margin: 2rem 0 2rem 0;
    p {
      background-color: ${({ theme }) => theme.colors["inputBg2"]};
      flex: 1;
      height: 0.1rem;
    }
    span {
      margin: 0 0.5rem;
      font-size: 1.125rem;
      line-height: 1.75rem;
      color: ${({ theme }) => theme.colors["inputBg2"]};
    }
  }
  .center {
    display: flex;
    flex-direction: column;
    row-gap: 1rem;
    margin: 0 auto;
    max-width: 50%;
    .alt-opt {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  }
`;

const SignUpScreen = () => {
  const { dispatch } = useContext(AppContext);

  const navigate = useNavigate();
  const handleModal = () => {
    dispatch({ type: "FALSE" });
  };
  const handleNavigate = () => {
    dispatch({ type: "TOGGLE" });
    navigate("/dashboard/profile");
  };

  const titles = ["Sheikh", "Alhaji", "Alhaja", "Prof"];
  return (
    <LoginContainer>
      <div className="heading">
        <Text fs="1.83rem" lh="2.6rem" color="black">
          Create an account
        </Text>{" "}
        <CloseIcon onClick={handleModal} />
      </div>
      <div className="input">
        <div className="title">
          <SelectMenu defaultValue="Title" options={titles} />
          <Input placeholder="Other title" />
        </div>
        <Input placeholder="Full name" />
        <Input placeholder="Email" />
        <Input placeholder="Phone number" />
        <Input placeholder="Password" />
        <Input placeholder="Confirm Password" />
      </div>
      <div className="check">
        <Checkbox text="Receive update from us" />
      </div>
      <div className="center">
        <Button title="Subscribe" wt="100%" onClick={handleNavigate} />

        <div className="alt-opt">
          <Text fs="1.125rem" lh="1.75rem" color="inputBg2">
            have account ?
          </Text>
          <Text fs="1.2175rem" fw={500} lh="1.83rem" color="primary">
            Login
          </Text>
        </div>
      </div>
    </LoginContainer>
  );
};

export default SignUpScreen;
