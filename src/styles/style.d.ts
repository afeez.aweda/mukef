import { themeType } from "./appTheme";
import {} from "styled-components"

declare module "styled-components" {
    interface DefaultTheme extends themeType {}
}