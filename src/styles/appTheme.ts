const appTheme = {
  colors: {
    primary: "#0B8EC2",
    secondary: "#1D2319",
    dark: "#585858",
    dark2: "#464646",
    black: "#111",
    active: "#fff",
    light: "#ffffff33",
    light2: "#ffffff7f",
    inputBg: "#B6B6B6",
    inputBg2: "#858585",
    text: "#5F5F5F",
  },
};
export const themeType = typeof appTheme;
export default appTheme;
