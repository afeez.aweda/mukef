import { createGlobalStyle } from "styled-components";

const globalStyles = createGlobalStyle`
html,body {
    padding: 0;
    margin: 0;
    font-family: 'Inter', 'Poppins', sans-serif;
    font-size: 1rem;
}

body{
    width: 100%;
    height: 100vh;
    overflow: hidden;
  
}

:root {
    font-size: 1rem;
    line-height: 1.5rem;
    font-synthesis: none;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-size-adjust: 100%;
}

*,*::before,*::after{
    box-sizing: border-box;
   
}

.center{
    max-width: 90%;
    margin: 0 auto;
   
}

img{
    max-width: 100%;
}

a{
text-decoration: none;
}

input:-webkit-autofill,
input:-webkit-autofill:hover,
input:-webkit-autofill:focus,
input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
    -webkit-text-fill-color: #000 !important;
}
`;

export default globalStyles;
