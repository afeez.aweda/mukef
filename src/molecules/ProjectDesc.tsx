import styled from "styled-components"
import { Heading, Text } from "../atoms/Typography"

const Wrapper = styled.div<{top?:boolean}>`
margin-bottom: ${({top}) => top ? '2rem' : '3.75rem'};
max-width: 43rem;
@media screen and (max-width: 900px) {
 .heading{
   font-size: 2.8rem;
   line-height: 3.7rem;
 }
}
@media screen and (max-width: 500px) {
 .text{
   font-size: 15px;
 }
}
`
const ProjectDesc = ({text, top}:{text?:string, top?:boolean}) => {
  return (
    <Wrapper top={top}>
      <Heading className="heading" color="#000" lh="5rem" fw={700} h1>
        {text}
      </Heading>
      <Text className="text" fs="18px">Hi. My name is Khoa. I am a Montreal-based filmmaker, stage director and video designer, author of feature films, documentaries, essays and video installations. I seek to create objects that blur the limits of the sacred.Hi. My name is Khoa. I am a Montreal-based filmmaker, stage director and video designer, author of feature films, documentaries, essays and video installations. I seek to create objects that blur the limits of the sacred.Hi. My name is Khoa. I am a Montreal-based filmmaker, stage director and video designer, author of feature films, documentaries, essays and video installations. I seek to create objects that blur the limits of the sacred.</Text>
    </Wrapper>
  )
}

export default ProjectDesc