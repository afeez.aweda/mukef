import styled from "styled-components";
import Image from "../atoms/image";
import { Heading } from "../atoms/Typography";

const StyledContainer = styled.div`
  padding: 2.6rem 0;
  .head-content {
    display: grid;
    grid-template-columns: 1.2fr 1fr;
    margin-bottom: 3rem;
  }
  .img {
    width: 100%;
  }
  @media screen and (max-width: 1000px) {
    padding: 1.5rem 0;
    .head-content {
      display: flex;
      flex-direction: column;
      text-align: center;
      margin-bottom: 1rem;
      .head {
        font-size: 2rem;
        font-weight: 500;
        line-height: 1.5rem;
        margin-bottom: 1rem;
      }
      .sub-head {
        font-size: 1.1rem;
        font-weight: 400;
        line-height: 1.5rem;
      }
    }
    .img {
      align-self: center;
      width: 100%;
    }
  }
`;

const Index = ({
  head,
  content,
  imgUrl,
}: {
  head: string;
  content: string;
  imgUrl: string;
}): JSX.Element => (
  <StyledContainer>
    <div className="head-content">
      <Heading color="black" h2 lh="3rem" className="head">
        {head}
      </Heading>
      <Heading fs="1.375rem" lh="1.875rem" color="black" className="sub-head">
        {content}
      </Heading>
    </div>
    <div className="img">
      <Image imgUrl={imgUrl} wt="100%" />
    </div>
  </StyledContainer>
);

export default Index;
