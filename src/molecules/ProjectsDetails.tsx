import styled from "styled-components"
import Button from "../atoms/button"
import ProjectDesc from "./ProjectDesc"
import ProjectsTemplate from "./ProjectsTemplate"

const Wrapper = styled.div<{top?:boolean}>`
  padding: ${({top}) => top?'0 0 3.6rem':'2.68rem 0 6.1rem'};
  background-color: ${({top}) => top?'#fff':'#F0F0F0'};
`
const Main = styled.div<{top?:boolean}>`
display: grid;
grid-template-columns: ${({top}) => top?'1fr 1fr':'1fr 1fr 1fr'};
grid-column-gap: 5%;

@media screen and (max-width: 900px){
  display: ${({top}) => top?'flex':'grid'};
  grid-template-columns: 1fr 1fr;
  flex-wrap: wrap; 
}

@media screen and (max-width: 500px){
  display: flex;
}
`

const ProjectsDetails = ({text, data, top}:{text?:string, data?:any, top?:boolean}) => {
  return (
    <Wrapper top={top}>
      <div className="center">
      <ProjectDesc top={top} text={text}/>
      <Main top={top}>
      {data.map(({image}:{image:any}, index:number) => <ProjectsTemplate key={index} mb={top? '2rem 0 2.5rem' : '1.25rem 0 2.5rem'} top={top} src={image}/>)}
      </Main>
      <Button title="See all"/>
    </div>
    </Wrapper>
  )
}

export default ProjectsDetails