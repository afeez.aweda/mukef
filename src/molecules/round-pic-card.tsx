import styled from "styled-components";
import Image from "../atoms/image";
import { Heading, Text } from "../atoms/Typography";

const StyledContainer = styled.div<{ wt?: string; ht?: string }>`
  height: ${({ ht }) => ht ?? "100%"};
  width: ${({ wt }) => wt ?? "100%"};
  .info {
    text-align: center;
  }
  @media screen and (max-width: 1044px) {
    max-width: 90%;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    margin-bottom: 1rem;
    .info {
      display: flex;
      flex-direction: column;
      row-gap: 0.5rem;
      .info-head {
        font-size: 1.2rem;
        line-height: 1.2rem;
        margin-top: 1rem;
        font-weight: 600;
      }
      .title,
      .desc {
        font-size: 1rem;
        line-height: 1.2rem;
      }
    }
  }
`;

const Index = ({
  name,
  title,
  imgUrl,
  desc,
  wt,
  ht,
  head,
}: {
  name: string;
  title?: string;
  imgUrl: string;
  desc: string;
  wt?: string;
  ht?: string;
  head?: boolean;
}) => {
  return (
    <StyledContainer wt={wt} ht={ht}>
      <Image imgUrl={imgUrl} />
      <div className="info">
        <Heading
          className="info-head"
          color="black"
          fs={head ? "1.2rem" : "1rem"}
          lh={head ? "1.57rem" : "1.25rem"}
        >
          {name}
        </Heading>
        {title && (
          <Heading
            color="dark"
            fs="0.875rem"
            lh="1.2rem"
            fw={600}
            className="title"
          >
            {title}
          </Heading>
        )}
        <Heading
          color="dark"
          fs="0.875rem"
          lh="1.2rem"
          fw={400}
          className="desc"
        >
          {desc}
        </Heading>
      </div>
    </StyledContainer>
  );
};

export default Index;
