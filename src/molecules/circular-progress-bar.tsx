import styled from "styled-components";
import { Heading, Text } from "../atoms/Typography";

const StyledContainer = styled.div<{ value: string }>`
  .col {
    width: 9.6rem;
    height: 9.6rem;
    background: #00000066;
    border-radius: 50%;
    .skill {
      width: 100%;
      height: 100%;
      position: relative;
      display: grid;
      place-items: center;
      border-radius: 50%;
      -webkit-border-radius: 50%;
      -moz-border-radius: 50%;
      -ms-border-radius: 50%;
      -o-border-radius: 50%;
      background: conic-gradient(
        #27ae60 ${({ value }) => value ?? "90%"},
        transparent 0
      );
      box-shadow: 0px 0px 0px 1px #dddedd;

      &::before {
        content: "";
        position: absolute;
        width: 78%;
        height: 78%;
        background: #cdcdcd;
        border-radius: 50%;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        -o-border-radius: 50%;
        box-shadow: 5px 5px 15px #c7c7c77d inset, -5px -5px 15px #c7c7c77d inset;
      }

      .content {
        position: absolute;
        text-align: center;
        user-select: none;
        display: grid;
        place-items: center;
        grid-gap: 0.4rem;
      }
    }
  }
`;

const CircularProgressBar = ({ value }: { value: string }) => {
  return (
    <StyledContainer value={value}>
      <div className="col">
        <div className="skill">
          <div className="content">
            <Heading fw={600} fs="1.1475rem" lh="1.375rem" color="black">
              {value ?? "90%"}
            </Heading>
            <Heading fs="0.765rem" lh="0.938rem" color="dark2" fw={400}>
              Complete
            </Heading>
          </div>
        </div>
      </div>
    </StyledContainer>
  );
};

export default CircularProgressBar;
