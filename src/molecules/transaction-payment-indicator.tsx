import styled from "styled-components";
import { Text } from "../atoms/Typography";

const StyledContainer = styled.div<{ color: string }>`
  display: flex;
  .indicator {
    height: 0.5rem;
    width: 0.5rem;
    background: ${({ color }) => color};
    margin-right: 0.5rem;
    margin-top: 0.34rem;
  }
  .amount {
    display: grid;
    gap: 1.1rem;
  }

  @media screen and (max-width: 530px) {
    .amount {
      gap: 0.3rem;
      .amt-title {
        font-size: 1rem;
      }
    }
  }
`;

const TransactionPaymentIndicator = ({
  title,
  amount,
  color,
}: {
  title: string;
  amount: string;
  color: string;
}): JSX.Element => (
  <StyledContainer color={color}>
    <div className="indicator"></div>
    <div className="amount">
      <Text lh="1.2rem" className="amt-title">
        {title}
      </Text>
      <Text
        fw={500}
        fs="1.25rem"
        lh="1.5rem"
        color="black"
        className="amt-value"
      >
        ₦{amount}
      </Text>
    </div>
  </StyledContainer>
);

export default TransactionPaymentIndicator;
