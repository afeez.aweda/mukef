import styled from 'styled-components'
import { Heading } from '../atoms/Typography'

const Top = styled.div`
  background-color: #1D2319;
  padding: 7.5rem 0 20.375rem;
  @media screen and (max-width: 450px){
    .heading{
      font-size: 2.3rem;
    }
    .heading1{
      font-size: 2.8rem;
    }
}
`
const ContactTop = ({text1, text2}:{text1?:string, text2?:string}) => {
  return (
      <Top>
        <div className='center'>
      <Heading className='heading' lh='3rem' h2>{text1}</Heading>
      <Heading className='heading1' mg='4rem 0 3.5rem' lh='3rem' h1 fw={700}>{text2}</Heading>
      </div>
      </Top>
  )
}

export default ContactTop
