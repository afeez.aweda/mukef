import styled from 'styled-components'
import Button from '../atoms/button'
import Input from '../atoms/input'
import { Text } from '../atoms/Typography'
import SubscribeCards from './SubscribeCards'

const Wrapper = styled.div`
margin-bottom: 4.5rem;
@media screen and (max-width: 880px){
    padding: 0;
}
`
const Inputs = styled.div`
  margin-bottom: 2.25rem;
  display: flex;
  column-gap: 1.13rem;
  row-gap: 1.34rem;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  @media screen and (max-width: 372px){
    row-gap: 0.7rem;
     Button{ width: 100%;}

}
`
const Inputt = styled.div<{width?:string}>`
  width: ${({width}) => width};
  @media screen and (max-width: 372px){
    width: 100%;
}`
const Save = styled.div`
display: flex;
margin-left: 17px;

input[type='checkbox']{
width: 18px !important;
height: 18px !important;
margin: 4px;
appearance: none;
border-radius: 3px;
text-align: center;
background: #B6B6B6;
}

input[type='checkbox']:checked:after {
content: '✔';
color: white;
}
`
const Select = styled.select<{indx?:number}>`
  background-color: #B6B6B6;
  border: none;
  outline: none;
  border-radius: 4.5px;
  width: 47%;
  padding: 1rem;
  font-size: 1rem;
  border-right: 12px solid transparent;
  @media screen and (max-width: 500px){
        padding: 0.8rem;
        font-size: 0.8rem;
  }
  @media screen and (max-width: 372px){
      width: 100%;
}
`
const Info = styled.div`
  width: 1.7rem;
  height: 1.7rem;
  border-radius: 50%;
  background-color: #B6B6B6;
  color: #fff;
  font-size: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Year = styled.div`
  width: 47%;
  padding: 11px;
  border-radius: 5px;
  background-color: #B6B6B6;
  color: #858585;
  display: flex;
  align-items: center;
  @media screen and (max-width: 372px){
      width: 100%;
}
`
const Slash = styled.div`
  width: 16%;
  border: 0.8px solid gray;
  transform: rotate(-60deg) translateY(100%);
`
const MiniInput = styled.input`
  background: #D0D0D0;
  border: 1px solid #858585;
  border-radius: 5px;
  height: 1.8rem;
  width: 24%;
  outline: none;
  text-align: center;
`
const CVV = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: 1rem;
  width: 45.3%;
  @media screen and (max-width: 372px){
      width: 100%;
}
`
const titleArray = [
  {title : 'Payment method'},
  {title : 'Mastercard'},
  {title : 'Visa'},
  {title : 'PayPal'},
]
const inputArray = [
  {placeholder: 'Cardholder'},
  {placeholder: 'Card number'},
]

const DonationCard = () => {
  return (
    <SubscribeCards text='Instant donation'>
    <Wrapper>
    <Inputs>
      <Select name="" id="">
        {titleArray.map(({title}, index) => 
        <option key={title} style={{color: '#858585'}} disabled={index === 0} value="">{title}</option>
        )}
      </Select>
        {inputArray.map(({placeholder}, index) => <Inputt key={placeholder} width={ index===1?'47%': '45.5%'}> <Input pd='14px' placeholder={placeholder}/></Inputt>)}
        <CVV>
        <Input pd='14px' width='80%' placeholder='CVV'/>
        <Info>?</Info>
        </CVV>
        <Year> <Text color='#858585' mg='0 10px 0 4px'>Expiry</Text> <MiniInput placeholder='M'/> <Slash/> <MiniInput placeholder='Y'/> </Year>
      <Button wt='45.5%' title='Donate'/>
        </Inputs>
        <Save>
        <input type='checkbox'/>
        <Text fs='14.56px' mg='0 0 0 13.6px'>Save my details for donations</Text>
      </Save>
    </Wrapper>
    </SubscribeCards>
  )
}

export default DonationCard