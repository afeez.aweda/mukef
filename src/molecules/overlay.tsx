import { ReactNode, useContext } from "react";
import styled from "styled-components";
import { AppContext } from "../context/context";
import { ModalProps } from "../utils/types";

const OverlayContainer = styled.div<Pick<ModalProps, "isActive">>`
  width: 100%;
  height: 100vh;
  display: ${({ isActive }) => (isActive ? "block" : "none")};
  position: fixed;
  background-color: #201f1f43;
  backdrop-filter: blur(0.5rem);
  transition: 0.5s;
  z-index: 1;
`;

const Overlay = ({ isActive }: Pick<ModalProps, "isActive">) => {
  const { dispatch } = useContext(AppContext);
  const handleOverlay = () => {
    dispatch({ type: "FALSE" });
  };
  return (
    <OverlayContainer
      isActive={isActive}
      onClick={handleOverlay}
    ></OverlayContainer>
  );
};

export default Overlay;
