import { useState } from "react";
import styled from "styled-components";

const SelectWrapper = styled.div<{ active: boolean; wt?: string }>`
  width: ${({ wt }) => wt ?? "300px"};
  label {
    text-transform: capitalize;
    font-size: 0.8775rem;
    line-height: 1.42rem;
    font-family: Poppins;
    color: ${({ theme }) => theme.colors["inputBg2"]};
  }
  .dropdown {
    position: relative;
    width: 100%;
    padding: 1.63rem;
    ::before {
      position: absolute;
      content: "";
      z-index: 10000;
      width: 10px;
      height: 10px;
      border: 2px solid #333;
      right: 20px;
      top: ${({ active }) => (active ? "22px" : "18px")};
      border-top: 2px solid ${({ theme }) => theme.colors["inputBg"]};
      border-right: 2px solid ${({ theme }) => theme.colors["inputBg"]};
      transform: ${({ active }) =>
        active ? "rotate(-225deg)" : "rotate(-45deg)"};
      transition: 0.05s;
      pointer-events: none;
    }
    .select {
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      cursor: pointer;
      background: ${({ theme }) => theme.colors["inputBg"]};
      border: none;
      outline: none;
      box-shadow: 0 5px 20px rgba(0, 0, 0, 0.05);
      padding: 1rem 1rem 0.5rem 1rem;
      border-radius: 0.3rem;
    }
    .option {
      z-index: 2;
      position: absolute;
      top: 55px;
      left: 0;
      width: 100%;
      background: ${({ theme }) => theme.colors["inputBg"]};
      box-shadow: 0 30px 30px rgba(0, 0, 0, 0.05);
      border-radius: 10px;
      overflow: hidden;
      display: ${({ active }) => (active ? "block" : "none")};
      div {
        padding: 12px 20px;
        cursor: pointer;
        :hover {
          background: #f2f2f2;
          color: ${({ theme }) => theme.colors["inputBg2"]};
        }
      }
    }
  }
  @media screen and (max-width: 500px) {
    .dropdown {
      padding: 1.3rem;
      font-size: 0.875rem;
      ::before {
        top: ${({ active }) => (active ? "20px" : "12px")};
      }
      .select {
        padding-top: 0.5rem;
      }
      .option {
        top: 45px;
        div {
          padding: 5px 20px;
        }
      }
    }
  }
`;

const SelectMenu = ({
  defaultValue,
  options = [],
  wt,
  label,
}: {
  defaultValue: string;
  options: Array<string>;
  wt?: string;
  label?: string;
}) => {
  const [toggle, setToggle] = useState(false);
  const [value, setValue] = useState(defaultValue);
  const handleSelect = (option: string) => () => setValue(option);

  const handleToggle = () => () => setToggle(!toggle);

  return (
    <SelectWrapper active={toggle} wt={wt}>
      {label && <label htmlFor={label}>{label}</label>}
      <div className="dropdown" onClick={handleToggle()}>
        <span className="select">{value}</span>
        <div className="option">
          {options.map((value) => (
            <div onClick={handleSelect(value)} key={value}>
              {value}
            </div>
          ))}
        </div>
      </div>
    </SelectWrapper>
  );
};

export default SelectMenu;
