import { useContext } from "react";
import styled from "styled-components";
import Button from "../atoms/button";
import Image from "../atoms/image";
import { Text } from "../atoms/Typography";
import { AppContext } from "../context/context";

const ProjectCardWrapper = styled.div`
  display: grid;
  row-gap: 1.2rem;
  .details {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    .desc {
      width: 55%;
    }
  }
  @media screen and (max-width: 1100px) {
    text-align: center;
    row-gap: 0.3rem;
    .details {
      flex-direction: column;
      align-items: center;
      row-gap: 1.4rem;
      .desc {
        width: 100%;
        font-size: 1rem;
      }
    }
  }
`;

const ProjectCard = ({
  imgUrl,
  name,
  description,
}: {
  imgUrl: string;
  name: string;
  description: string;
}) => {

  const { dispatch } = useContext(AppContext);
  const handleEditProfile = () => {
    dispatch({ modal: "support-project" });
  };

  return (
    <ProjectCardWrapper>
      <Image imgUrl={imgUrl} />
      <Text fs="1.5rem" fw={500} lh="1.9rem" color="black">
        {name}
      </Text>
      <div className="details">
        <Text fs="1.125rem" lh="1.76rem" color="black" className="desc">
          {description}
        </Text>
        <Button title="support this project" border color="primary" outline onClick={handleEditProfile} />
      </div>
    </ProjectCardWrapper>
  );
};

export default ProjectCard;
