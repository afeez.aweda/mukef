import styled from "styled-components";
import Image from "../atoms/image";
import { Text } from "../atoms/Typography";

const StyledWrapper = styled.div`
  margin-bottom: 2.6rem;
`;

const RecentNewsCard = ({
  imgUrl,
  content,
}: {
  imgUrl: string;
  content: string;
}) => {
  return (
    <StyledWrapper>
      <Image imgUrl={imgUrl} />
      <Text color="dark" fw={500} fs="1.5rem" mg="1.4rem 0 0 1rem">
        {content}
      </Text>
    </StyledWrapper>
  );
};

export default RecentNewsCard;
