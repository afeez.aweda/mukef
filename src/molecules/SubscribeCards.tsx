import styled from "styled-components"
import { Text } from "../atoms/Typography"

const Wrapper = styled.div`
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0px -3.62791px 13.6047px rgba(255, 255, 255, 0.39), 0px 3.62791px 9.97674px rgba(29, 35, 25, 0.35);
  height: fit-content;
`
const SubscribeCards = ({text, children}:{text?:string, children?:any}) => {
  return (
    <Wrapper>
    <div className="center">
    <Text fs='21.77px' mg="2.77rem 0">{text}</Text> 
    {children}
    </div>
    </Wrapper>
  )
}

export default SubscribeCards