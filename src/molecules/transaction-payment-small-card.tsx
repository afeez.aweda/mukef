import styled from "styled-components";
import { Heading } from "../atoms/Typography";

interface MiniCardProps {
  color?: string;
  title: string;
  amount: string;
}

const StyledContainer = styled.div<Omit<MiniCardProps, "title" | "amount">>`
  max-height: 9.25rem;
  width: 100%;
  padding: 1.5rem 2.5rem 2.375rem 2.5rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 1.5rem;
  border-radius: 0.625rem;
  border: 1px solid #e9ebfdb2;
  background: #1d231926;

  .title {
    padding: 0.5rem 0.75rem;
    background: ${({ color }) => `${color}14
`};
    color: ${({ color }) => color};
    font-weight: 500;
    font-size: 0.875rem;
    line-height: 1.05rem;
    border-radius: 0.3125rem;
    align-self: flex-start;
  }

  @media screen and (max-width: 750px) {
    max-height: 100%;
    padding: 1rem 1rem;
    .title {
    }
    .amount {
      font-size: 1.2rem;
    }
  }
`;

const TransactionPaymentSmallCard = ({
  amount,
  title,
  color,
}: MiniCardProps) => {
  return (
    <StyledContainer color={color}>
      <span className="title">{title}</span>
      <Heading
        fs="1.5rem"
        fw={700}
        lh="1.8rem"
        color="black"
        className="amount"
      >
        ₦{amount}
      </Heading>
    </StyledContainer>
  );
};

export default TransactionPaymentSmallCard;
