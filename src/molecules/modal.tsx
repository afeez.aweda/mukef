import styled, { css } from "styled-components";
import { ModalProps } from "../utils/types";

const ModalContainer = styled.div<ModalProps>`
  background-color: #fff;
  position: absolute;
  left: 50%;
${({ ht }) =>
  ht &&
  css`
    height: 41.625rem;
    overflow: hidden;
    padding: 2rem 1.5rem;
    @media screen and (max-width: 500px) {
      height: 50%;
    }
  `}
  top: ${({ isActive }) => (isActive ? "50%" : "40%")};
  border-radius: 1.25rem;
  visibility: ${({ isActive }) => (isActive ? "visible" : "hidden")};
  opacity: ${({ isActive }) => (isActive ? 1 : 0)};
  transform: translate(-50%, -50%);
  transition: 0.5s;
  filter: drop-shadow(-4px 10px 42px rgba(0, 0, 0, 0.25));
  z-index: 2;
  
`;

const Modal = ({ isActive, children, ht }: ModalProps) => {
  return (
    <ModalContainer isActive={isActive} ht={ht}>
      <div>{children}</div>
    </ModalContainer>
  );
};

export default Modal;
