import styled, { css } from "styled-components"
import Image from "../atoms/image"
import { Text } from "../atoms/Typography"

const Wrapper = styled.div<{mb?:string; top?:boolean}>`
  ${({mb}) => mb && css`
    margin: ${mb};
  `}
`
const ProjectsTemplate = ({src, top, mb}: {src?:any, top?:boolean, mb?:string}) => {
  return (
    <Wrapper mb={mb}>
      <div>
      <Image imgUrl={src}/>
      </div>
      <Text mg="1rem 0 0"><span style={{fontWeight:600, fontSize:`${top?'29px':'19px'}`}}>Muhammad Kamaldeen University project </span><span style={{fontWeight:500, fontSize:`${top?'20px':'16px'}`, color: '#5F5F5F'}}> - Hi. My name is Khoa. I am a </span></Text>
    </Wrapper>
  )
}
export default ProjectsTemplate