import styled, { css } from "styled-components"
import Image from "../atoms/image"
import { Text } from "../atoms/Typography"
import Avatar from "../vectors/images/avatar.png"

const Wrapper = styled.div<{ top?: boolean }>`
  display: ${({top}) => top ? 'grid' : 'flex' };
  grid-template-columns: 0.7fr 0.35fr;
  flex-direction: column;
  margin-bottom: 3.75rem;
  ${({top}) => top && css`
    column-gap: 4%;
  `}
  @media screen and (max-width: 1200px){
    display: flex;
  }
` 
const ContentWrapper = styled.div<{top?: boolean}>`
  width: ${({top}) => top ? '' : '90%' };
  margin-top: ${({top}) => top ? '' : '1.5rem' };

  @media screen and (max-width: 600px){
    .text{font-size: 15px}
  }
`
const Info = styled.div`
  display: flex;
  align-items: center;
`
const DateTextWrapper = styled.div`
  display: flex;
  align-items: center;
  @media screen and (max-width: 768px){
    .text{font-size: 12px}
  }
`

const NewsTemplate = ({ src, top }: { src?: any; top?: boolean }) => {
  return (
    <Wrapper top={top}>
      <Image imgUrl={src}/>
      <ContentWrapper top={top}>
        <DateTextWrapper><Text className="text" fs="14px">RECENT NEWS</Text><div style={{width: '3px', height: '3px', background: '#000', margin:'0 0.5rem'}}></div> <Text className="text" color="#5F5F5F" fs="14px"> May 17 2022</Text></DateTextWrapper>
        <Text fs={top?"28px":'24px'} lh="1.5rem" fw={600} mg="8px 0 1rem">Muhammad Kamaldeen University project</Text>
        <Text fs="22px" fw={500}> - What you need to know.</Text>
        <Text className="text" lh="28px" fs="18px" mg="8px 0 23px">Hi. My name is Khoa. I am a Montreal-based filmmaker, stage director and video designer, author of feature films, documentaries, essays and video installations. I seek to create objects that blur the limits of the sacred.</Text>
        <Info>
        <img src={Avatar}/>
        <div style={{marginLeft: '1rem'}}>
          <Text fs="18px" fw={500}>Marvin McKinney</Text>
          <Text fs="14px" >Co-founder</Text>
        </div>
        </Info>
        
      </ContentWrapper>
    </Wrapper>
  );
};

export default NewsTemplate;