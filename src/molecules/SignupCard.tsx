import styled from 'styled-components'
import ButtonEl from '../atoms/button'
import Input from '../atoms/input'
import { Text } from '../atoms/Typography'
import SubscribeCards from './SubscribeCards'

const Wrapper = styled.div`
margin-bottom: 1.9rem;

.btm{
  display: flex;
  align-items: center;
  justify-content: space-between;
  column-gap: 1rem;
  row-gap: 1rem;
}
@media screen and (max-width: 372px){
  .btm{
    display: flex;
    flex-direction: column-reverse;
  }
}
`
const Inputs = styled.div`
  margin-bottom:2rem;
  display: flex;
  column-gap: 1.13rem;
  row-gap: 1.34rem;
  flex-wrap: wrap;
  justify-content: space-between;
  @media screen and (max-width: 372px){
    row-gap: 0.7rem;
    Select{
      width: 100%;
    }
}
`
const Inputt = styled.div<{width?:string}>`
  width: ${({width}) => width};
  @media screen and (max-width: 372px){
    width: 100%;
}
`
const Button = styled.button`
  font-size: 15px;
  color: #0B8EC2;
  border: none;
  background-color: transparent;
  font-weight: 800;
  cursor: pointer;
`
const Save = styled.div`
  display: flex;
  margin-left: 25px;

  input[type='checkbox']{
  width: 18px !important;
  height: 18px !important;
  margin: 4px;
  appearance: none;
  border-radius: 3px;
  text-align: center;
  background: #B6B6B6;
}

input[type='checkbox']:checked:after {
  content: '✔';
  color: white;
}
@media screen and (max-width: 372px){
  margin-left: 0;
}
`
const Select = styled.select<{indx?:number}>`
  background-color: #B6B6B6;
  border: none;
  outline: none;
  border-radius: 4.5px;
  width: 26%;
  padding: 0.5rem;
  font-size: 1rem;
  border-right: 10px solid transparent;
`

const titleArray = [
  {title : 'Title'},
  {title : 'Sheikh'},
  {title : 'Alhaji'},
  {title : 'Alhaja'},
  {title : 'Prof.'},
]
const inputArray = [
  {placeholder: 'Full name'},
  {placeholder: 'Email'},
  {placeholder: 'Phone number'},
  {placeholder: 'Phone number 2'},
  {placeholder: 'Password'},
  {placeholder: 'Confirm Password'},
]
const SignupCard = () => {
  return (
    <SubscribeCards text='Create an account'>
    <Wrapper>
      <Inputs>
      <Select name="" id="">
        {titleArray.map(({title}, index) => 
        <option key={title} style={{color: index === 0? '#fff' : '#000'}} disabled={index === 0} value="">{title}</option>
        )}
      </Select>
        {inputArray.map(({placeholder}, index) => <Inputt key={placeholder} width={index === 0? '68%' : index === 1 ? '100%': '47%'}> <Input pd='10px' placeholder={placeholder}/></Inputt>)}
      </Inputs>
      <div className='btm'>
      <Save>
        <input type='checkbox'/>
        <Text fs='14px' mg='0 0 0 10px'>Receive update form us</Text>
      </Save>
      <ButtonEl wt='48%' title='Subscribe'/>
      </div>
      <div style={{textAlign: 'center', marginTop: '2.1rem'}}>
      <Text >If you’re already a member, you can <Button>Login</Button> </Text>
      </div>
    </Wrapper>
    </SubscribeCards>
  )
}

export default SignupCard