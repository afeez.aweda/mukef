import styled from "styled-components";
import useWindowDimensions from "../hooks/useDimensionHook";
import { objectDatasType } from "../utils/types";
import AccordionIcon from "../vectors/accordion";
import BookIcon from "../vectors/table-book-icon";
import TableDataSortIcon from "../vectors/table-data-sort-icon";

const T = styled.table`
  width: 100%;

  thead {
    width: 100%;
    tr {
      background: #f2f2f2;
      border-radius: 0.3125rem;
      display: table;
      table-layout: fixed;
      width: 100%;
      padding: 0.8rem 1.5rem;
    }
  }
  @media screen and (max-width: 1080px) {
    width: 100%;
    thead {
      width: 100%;
      tr {
        padding: 0.5rem 1rem;
      }
    }
  }
  @media screen and (max-width: 990px) {
    thead {
      display: none;
    }
    overflow: auto;
    width: 100%;
  }
`;

const Th = styled.th<{ alignText?: string; wt?: number; status?: string }>`
  border: none;
  font-family: Poppins;
  font-size: 0.875rem;
  font-weight: 500;
  line-height: 1.125rem;
  text-transform: uppercase;
  width: ${({ wt }) => wt && `${wt}px`};
  span {
    display: flex;
    column-gap: 0.5rem;
    justify-content: ${({ alignText }) =>
      alignText ? alignText : "flex-start"};
  }
  @media screen and (max-width: 1080px) {
    font-size: 0.8rem;
  }
  @media screen and (max-width: 909px) {
    font-size: 0.7rem;
  }
  @media screen and (max-width: 740px) {
    font-size: 0.7rem;
  }
`;

const Tr = styled.tr<{}>`
  display: table;
  table-layout: fixed;
  width: 100%;
  padding: 0.8rem 1.5rem;
  &:nth-child(even) {
    background: #f9f9f9;
  }
  @media screen and (max-width: 1080px) {
    padding: 0.5rem 1rem;
  }
  @media screen and (max-width: 990px) {
    padding: 0;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    margin-bottom: 1.5rem;
    border: solid 1px lightgray;
    border-radius: 0.2rem;
    &:nth-child(even) {
      background: transparent;
    }
  }
`;

const Td = styled.td<{ alignText?: string; status?: string; wt?: string }>`
  text-transform: capitalize;
  &:last-child {
    text-align: center;
  }
  border-radius: ${({ status }) =>
    status === "success" || status === "failed" ? "1rem" : 0};

  color: ${({ status }) =>
    status === "success"
      ? "#2EB872"
      : status === "failed"
      ? "#F2994A"
      : "black"};
  background-color: ${({ status }) =>
    status === "success"
      ? "#2EB8721A"
      : status === "failed"
      ? "#F2994A1A"
      : ""};

  @media screen and (max-width: 1080px) {
    font-size: 0.8rem;
  }

  @media screen and (max-width: 990px) {
    width: 100%;
    display: flex;
    justify-content: flex-end;
    position: relative;
    padding: 0.8rem;
    border-radius: ${({ status }) => status && 0};
    font-size: 0.875rem;
    :last-child {
      border-bottom: none;
    }
    &:nth-child(even) {
      background: #f9f9f9;
    }
    ::before {
      position: absolute;
      content: attr(data-title);
      left: 0;
      width: 50%;
      font-size: 0.9rem;
      font-weight: 600;
      text-align: left;
      padding-left: 0.5rem;
      color: black;
    }
  }
`;

const Table = ({
  columns,
  datas,
}: {
  columns: Record<string, Record<string, string>>;
  datas?: Array<objectDatasType>;
}) => {
  const headerColumn = () =>
    Object.keys(columns).map((key) => (
      <Th
        key={`column-header-key-${key}`}
        status={key}
        alignText={columns[key].align as string}
        style={{ width: columns[key].wt as string }}
      >
        <span>
          {columns[key].label}
          {columns[key].icon === "toggle" ? <TableDataSortIcon /> : null}
        </span>
      </Th>
    ));
  const cell = (item: objectDatasType, key: string) => {
    const { width: wit } = useWindowDimensions();

    return (
      <Td
        data-title={columns[key].label}
        status={item[key] as string}
        style={{ width: wit <= 990 ? "100%" : (columns[key].wt as string) }}
        key={key}
      >
        {item[key] === "book" ? (
          <BookIcon />
        ) : item[key] === "accordion" ? (
          <AccordionIcon />
        ) : (
          (item[key] as string)
        )}
      </Td>
    );
  };

  const rows = (item: objectDatasType, i: number) => (
    <Tr key={`row-data-${i}`}>
      {Object.keys(columns).map((key) => cell(item, key))}
    </Tr>
  );

  return (
    <T>
      <thead className="thead">
        <tr className="tr">{headerColumn()}</tr>
      </thead>
      <tbody>{datas?.map((data, i) => rows(data, i))}</tbody>
    </T>
  );
};

export default Table;
