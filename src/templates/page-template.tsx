import { ReactNode, useContext } from "react";

import styled from "styled-components";

import { AppContext } from "../context/context";
import Modal from "../molecules/modal";
import Overlay from "../molecules/overlay";

import EditProfile from "../organisms/edit-profile";
import Footer from "../organisms/footer";
import LoginScreen from "../organisms/login";
import NavBar from "../organisms/nav-bar";
import SignUpScreen from "../organisms/sign-up";
import SupportProject from '../organisms/support-project'

const BodyWrapper = styled.div<{ noFlow?: boolean }>`
  height: 100vh;
  position: relative;
  display: flex;
  flex-direction: column;
  overflow: auto;
  overflow-x: hidden;
  main {
    overflow: ${({ noFlow }) => noFlow && "auto"};
  }
  .login {
  }
`;

const PageLayout = ({
  children,
  noFlow,
}: {
  children: ReactNode;
  noFlow?: boolean;
}) => {
  const {
    state: { isModal, modalType },
  } = useContext(AppContext);

  const renderModal = () => {
    if (isModal && modalType === "login") {
      return <LoginScreen />;
    } else if (isModal && modalType === "signup") {
      return <SignUpScreen />;
    } else if (isModal && modalType === "edit-profile") {
      return <EditProfile />;
    } else if (isModal && modalType === "support-project") {
      return <SupportProject />;
    }else {
      return null;
    }
  };

  return (
    <BodyWrapper noFlow={isModal || noFlow}>
      <NavBar />
      <Modal ht={modalType === "edit-profile"} isActive={isModal}>
        {renderModal()}
      </Modal>
      <Overlay isActive={isModal} />
      <main>{children}</main>
      {!noFlow && <Footer />}
    </BodyWrapper>
  );
};

export default PageLayout;
