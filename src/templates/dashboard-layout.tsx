import { Link, useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components";
import PageLayout from "./page-template";
import LogOutIcon from "../vectors/log-out-icon";
import ProfileIcon from "../vectors/profile-icon";
import ProjectSupportIcon from "../vectors/project-support-icon";
import TransactionIcon from "../vectors/transaction-icon";
import { Heading, Text } from "../atoms/Typography";
import { ReactNode } from "react";
import Image from "../atoms/image";
import Title from "../atoms/title";

const DashboardContainer = styled.div`
  height: 100%;
  overflow: hidden;
  display: grid;
  grid-template-columns: 18rem auto;

  aside {
    padding: 2rem 1.6rem;
    box-shadow: -4px 10px 42px #0000003f, -10px 4px 42px 18px #67afd83d;
    .icon-container {
      display: flex;
      padding: 0.75rem 2.5rem 0.75rem 1rem;
      align-items: center;
      column-gap: 1rem;
    }
    .isActive {
      background: #77777728;
      border-radius: 0.3125rem;
    }
  }
  .dashboard-content {
    height: 100%;
    overflow: auto;
    display: flex;
    flex-direction: column;
    max-width: 90%;
    margin: 0 auto;
    width: 100%;
    padding: 2rem;
    ::-webkit-scrollbar {
      display: none;
    }

    & {
      -ms-overflow-style: none;
      scrollbar-width: none;
    }
    .user {
      padding: 1.125rem 1.75rem;
      display: flex;
      align-items: center;
      column-gap: 1rem;
      border: 1px solid #e7e7e7;
      border-radius: 0.3125rem;
      margin-bottom: 2rem;
      .img {
        width: 6rem;

        border-radius: 50%;
      }
      .info {
        text-align: center;
      }
    }
    .child {
      margin: 2rem 0;
    }
  }

  @media screen and (max-width: 1300px) {
    grid-template-columns: 5rem auto;
    transition: grid-template-columns 500ms ease-in-out 200ms;
    aside {
      padding: 2rem 1rem;
      .icon-container {
        padding: 0.75rem 0.7rem;
        .icon-label {
          display: none;
        }
      }
    }
  }

  @media screen and (max-width: 500px) {
    display: flex;
    flex-direction: column-reverse;

    aside {
      position: fixed;
      width: 100%;
      z-index: 20;
      background-color: #fff;
      display: flex;
      justify-content: space-between;
      padding: 1rem;
      .icon-container {
        padding: 0.75rem 0.7rem;
        .icon-label {
          display: none;
        }
      }
    }
    .dashboard-content {
      padding: 1rem 0.2rem;
      padding-bottom: 5rem;
      .user {
        padding: 0.5rem;

        .img {
          width: 5rem;
        }

        .username {
          font-size: 0.95rem;
        }
        .title {
          font-size: 0.85rem;
        }
      }
    }
  }
`;

const DashboardLayout = ({
  children,
  title,
}: {
  children: ReactNode;
  title?: string;
}) => {
  const dashBoardNav = [
    { icon: <ProfileIcon />, name: "Profile", path: "/dashboard/profile" },
    {
      icon: <ProjectSupportIcon />,
      name: "Support a project",
      path: "/dashboard/support",
    },
    {
      icon: <TransactionIcon />,
      name: "Transactions",
      path: "/dashboard/transactions",
    },
    { icon: <LogOutIcon />, name: "Log Out" },
  ];

  const { pathname } = useLocation();
  const navigate = useNavigate();
  const handleLogout = () => {
    navigate("/");
  };
  return (
    <PageLayout noFlow={true}>
      <DashboardContainer>
        <aside>
          {dashBoardNav.map(({ icon, name, path }, i) =>
            i === dashBoardNav.length - 1 ? (
              <div className="icon-container" key={name} onClick={handleLogout}>
                {icon}{" "}
                <Text color="black" className="icon-label">
                  {name}
                </Text>
              </div>
            ) : (
              <Link
                to={path as string}
                className={`${path === pathname && "isActive"} icon-container`}
                key={name}
              >
                {icon}{" "}
                <Text color="black" className="icon-label">
                  {name}
                </Text>
              </Link>
            )
          )}
        </aside>
        <div className="dashboard-content">
          <div className="user">
            <div className="img">
              <Image imgUrl="/images/Frame 20 (1).png" />
            </div>
            <div className="info">
              <Heading
                fs="0.875rem"
                lh="1.25rem"
                color="black"
                className="username"
              >
                Prof. Abdul-Ganiyy Oladosu (AGAS) OON
              </Heading>
              <Heading
                fw={600}
                fs="0.75rem"
                lh="1.2rem"
                color="dark2"
                className="title"
              >
                Chief Imam of University of Ilorin
              </Heading>
            </div>
          </div>
          {title && <Title title={title as string} />}
          <div className="child">{children}</div>
        </div>
      </DashboardContainer>
    </PageLayout>
  );
};

export default DashboardLayout;
