import styled from "styled-components";

const StyledCheckBoxContainer = styled.label`
  display: block;
  position: relative;
  padding-left: 35px;
  cursor: pointer;
  font-family: Poppins;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    &:checked ~ .checkmark {
      background-color: ${({ theme }) => theme.colors["inputBg"]};
    }
  }
  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 1.52rem;
    width: 1.64rem;
    background-color: ${({ theme }) => theme.colors["inputBg"]};
  }
  &:hover input ~ .checkmark {
    background-color: ${({ theme }) => theme.colors["inputBg"]};
  }
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }
  input:checked ~ .checkmark:after {
    display: block;
  }
  .checkmark:after {
    left: 9px;
    top: 3px;
    width: 8px;
    height: 14px;
    border: 1px solid ${({ theme }) => theme.colors["inputBg2"]};
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  @media screen and (max-width: 500px) {
    font-size: 0.875rem;
    .checkmark {
      height: 1.1rem;
      width: 1.2rem;
      margin-top: 2px;
      :after {
        left: 6px;
        top: 1px;
        width: 6px;
        height: 12px;
      }
    }
  }
`;

const Checkbox = ({
  text,
  onClick,
}: {
  text: string;
  onClick?: () => void;
}) => {
  return (
    <StyledCheckBoxContainer onClick={onClick}>
      <input type="checkbox" /> {text}
      <span className="checkmark"></span>
    </StyledCheckBoxContainer>
  );
};

export default Checkbox;
