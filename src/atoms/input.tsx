import styled from "styled-components";
import SearchIcon from "../vectors/search-icon";

export const Wrapper = styled.div<{
  bg?: string;
  width?: string;
  border?: string;
  pd?: string;
  mg?: string;
  height?: string;
}>`
  label {
    margin-bottom: 1.875rem;
    text-transform: capitalize;
    font-size: 0.8775rem;
    line-height: 1.42rem;
    font-family: Poppins;
    color: ${({ theme }) => theme.colors["inputBg2"]};
  }
  width: ${({ width }) => width ?? "100%"};
  .input-wrapper {
    display: flex;
    align-items: center;
    column-gap: 0.5rem;
    background-color: ${({ bg, theme }) => theme.colors[bg ?? "inputBg"]};
    padding: ${({ pd }) => pd ?? "0.85rem"};
    margin: ${({ mg }) => mg};
    height: ${({ height }) => height};
    width: 100%;
    border-radius: 0.3125rem;
    border: ${({ border }) => border};
    input {
      font-family: "Poppins";
      font-size: 1rem;
      background: transparent;
      outline: none;
      width: 100%;
      border: none;
      ::placeholder {
        color: ${({ theme }) => theme.colors["black"]};
      }
    }
  }
  @media screen and (max-width: 500px) {
    .input-wrapper {
      padding: 0.6rem;
      input {
        font-size: 0.85rem;
      }
    }
  }
`;
const Input = ({
  placeholder,
  width,
  bg,
  border,
  pd,
  type,
  name,
  mg,
  label,
  prefix,
}: {
  placeholder?: string;
  width?: string;
  bg?: string;
  border?: string;
  pd?: string;
  type?: string;
  name?: string;
  mg?: string;
  prefix?: boolean;
  label?: string;
}) => {
  return (
    <Wrapper pd={pd} mg={mg} width={width} bg={bg} border={border}>
      {label && <label htmlFor={name}>{label}</label>}
      <div className="input-wrapper">
        {prefix && <SearchIcon />}
        <input type={type ?? "text"} name={name} placeholder={placeholder} />
      </div>
    </Wrapper>
  );
};

export default Input;
