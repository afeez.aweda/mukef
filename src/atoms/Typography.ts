import styled from "styled-components";
import { TextProps } from "../utils/types";

export const Heading = styled.h1<TextProps>`
  color: ${({ theme, color }) => theme.colors[(color as string) ?? "active"]};
  font-family: "Inter", sans-serif;
  font-weight: ${({ fw }) => fw ?? 500};
  font-size: ${({ h1, h2, fs }) => (h1 ? "3.5rem" : h2 ? "3rem" : fs)};
  margin: ${({ mg }) => mg ?? 0};
  padding: ${({ pd }) => pd ?? 0};
  text-align: ${({ textAlign }) => textAlign};
  line-height: ${({ lh }) => lh};
  width: ${({ width }) => width};
  line-height: ${({ height }) => height};
`;

export const Text = styled.span<TextProps>`
  display: block;
  font-family: "Poppins", sans-serif;
  color: ${({ theme, color }) => theme.colors[(color as string) ?? "dark2"]};
  font-weight: ${({ fw }) => fw ?? 400};
  font-size: ${({ fs }) => fs ?? "1rem"};
  padding: ${({ pd }) => pd};
  margin: ${({ mg }) => mg};
  line-height: ${({ lh }) => lh};
  text-align: ${({ textAlign }) => textAlign};
  user-select: none;
  width: ${({ width }) => width};
  text-decoration: ${({ decoration }) => decoration};
  :hover {
    color: ${({ theme, hoverColor }) => theme.colors[hoverColor as string]};
  }

  @media screen and (max-width: 768px) {
    font-size: ${({ fs }) => "1.2rem"};
  }
`;
