import styled from "styled-components";
import { ButtonProps } from "../utils/types";

const StyledButton = styled.button<ButtonProps>`
  background: ${({ outline, theme }) =>
    outline ? theme.colors["active"] : theme.colors["primary"]};
  border-radius: 0.3125rem;
  border: ${({ border, theme }) =>
    border ? `1px solid ${theme.colors.primary}` : "none"};
  width: ${({ wt }) => wt ?? "16.375rem"};
  height: ${({ hg }) => hg ?? "3.125rem"};
  font-family: "Poppins";
  outline: none;
  color: ${({ color, theme }) =>
    color ? theme.colors[color as string] : theme.colors["active"]};
  font-size: 1rem;
  font-weight: 600;
  margin: ${({ mg }) => mg};
  padding: ${({ pd }) => pd};
  &:hover {
    border: ${({ theme }) => `1px solid ${theme.colors.primary}`};
  }
`;

const Button = ({
  title,
  outline,
  onClick,
  type,
  mg,
  wt,
  color,
  border,
}: ButtonProps) => {
  return (
    <StyledButton
      outline={outline}
      onClick={onClick}
      type={type ? type : "button"}
      mg={mg}
      color={color}
      wt={wt}
      border={border}
    >
      {title}
    </StyledButton>
  );
};

export default Button;
