import styled from "styled-components";
import { Text } from "./Typography";

const StyledContainer = styled.div`
  width: 100%;
  border-bottom: 1px solid #f5f5f5;
  padding: 0.5rem 0;
`;

const Title = ({ title }: { title: string }) => {
  return (
    <StyledContainer>
      <Text fs="1.25rem" lh="1.5rem" color="black">
        {title}
      </Text>
    </StyledContainer>
  );
};

export default Title;
