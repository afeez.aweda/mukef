import styled from "styled-components";
import { ImgProps } from "../utils/types";

const Imagewrapper = styled.div<Omit<ImgProps, "imgUrl" | "alt">>`
  height: ${({ ht }) => ht ?? "100%"};
  width: ${({ wt }) => wt ?? "100%"};
  border-radius: ${({ round }) => (round ? "50%" : "0.625rem")};
  img {
    height: auto;
    width: ${({ wt }) => wt ?? "100%"};
    max-width: 100%;
    object-fit: cover;
  }
`;

const Image = ({ wt, ht, round, imgUrl, alt }: ImgProps) => {
  return (
    <Imagewrapper wt={wt} ht={ht} round={round}>
      <img src={imgUrl} alt={alt} />
    </Imagewrapper>
  );
};

export default Image;
