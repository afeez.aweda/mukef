import React from 'react'
import styled from 'styled-components'
import ContactTop from '../molecules/ContactTop'
import DonationCard from '../molecules/DonationCard'
import SignupCard from '../molecules/SignupCard'
import PageLayout from '../templates/page-template'

const Bottom = styled.div`
  background-color: #D9D9D9;
  padding-bottom: 28rem;
  position: relative;
  @media screen and (max-width: 775px){
    padding-bottom: 55rem;
}
@media screen and (max-width: 372px){
    padding-bottom: 75rem;
}
  `
const BottomCards = styled.div`
  display: grid;
  width: 90%;
  grid-template-columns: 0.42fr 0.48fr;
  justify-content: space-between;
  position: absolute;
  top: -14.5rem;

  @media screen and (max-width: 775px){
    display: flex;
    flex-direction: column;
    row-gap: 4rem;
}
`
const Subscribe = () => {
  return (
    <PageLayout>
    <div>
      <ContactTop text1='WANT TO BE PART OF US ?' text2='JOIN US OUR PLATFORM'/>
      <Bottom>
      <div className='center'>
        <BottomCards>
        <DonationCard/>
        <SignupCard/>
        </BottomCards>
      </div>
      </Bottom>
    </div>
    </PageLayout>
  )
}

export default Subscribe