import styled from "styled-components";
import PageLayout from "../templates/page-template";
import { Heading } from "../atoms/Typography";
import VisionCard from "../molecules/vision-and-mission-card";
import RoundPicCard from "../molecules/round-pic-card";
import Image from "../atoms/image";

const AboutUsContainer = styled.div`
  height: 100%;
  .section-1 {
    background: ${({ theme }) => theme.colors["secondary"]};
    padding: 2rem 0;
    .hero {
      display: grid;
      grid-template-columns: 0.6fr 0.5fr;
      .img {
        justify-self: end;
      }
    }
  }
  .section-2 {
    background: #d8d8d8;
  }
  .section-3 {
    background: ${({ theme }) => theme.colors["active"]};
    padding: 5.75rem 0;
    .content {
      .head {
        display: flex;
        justify-content: space-evenly;
        padding: 4rem 0;
      }
      .body {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
        column-gap: 2.9rem;
        row-gap: 3rem;
      }
    }
  }

  .section-2 {
    .content {
      display: flex;
      flex-direction: column;
    }
  }
  @media screen and (max-width: 1044px) {
    .section-1 {
      .hero {
        display: flex;
        flex-direction: column;
        row-gap: 2rem;
      }
      .img {
        align-self: center;
      }
      .content {
        display: flex;
        flex-direction: column;
        text-align: center;
        .head {
          font-size: 2rem;
          font-weight: 500;
          line-height: 1.5rem;
          margin-bottom: 1.5rem;
        }
        .sub-head {
          font-size: 1.1rem;
          font-weight: 400;
          line-height: 1.5rem;
        }
      }
    }
    .section-3 {
      .content {
        display: flex;
        flex-direction: column;
        text-align: center;
        .head {
          flex-direction: column;
          padding: 2rem 0;
          margin-bottom: 0;
        }
        .body {
          row-gap: 1.5rem;
        }
      }
    }
  }
  /* @media screen and (max-width: 1020px) {
    .hero {
      display: flex;
      flex-direction: column;
    }
  } */
`;

const AboutUs = () => {
  const missionAndVision = [
    {
      head: "Our Vision",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum sodales sed tellus leo non vestibulum, tellus vitae. Feugiat ornare nisl id sed dictumst. Interdum neque, in euismod euismod. Neque pretium rhoncus quis id feugiat vitae.",
      imgUrl: "./images/image 82.png",
    },
    {
      head: "Our Mission",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum sodales sed tellus leo non vestibulum, tellus vitae. Feugiat ornare nisl id sed dictumst. Interdum neque, in euismod euismod. Neque pretium rhoncus quis id feugiat vitae.",
      imgUrl: "./images/image 51.png",
    },
  ];
  const executivesHeading = [
    {
      imgUrl: "./images/Frame 22.png",
      name: "Sheikh Muhammad Kamalu-deen Al-Adabyy",
      desc: "Co-founder",
    },
    {
      imgUrl: "./images/Frame 19.png",
      name: "Sheikh Muhammad Kamalu-deen Al-Adabyy",
      desc: "Co-founder",
    },
  ];
  const executivesBody = [
    {
      imgUrl: "./images/Frame 21.png",
      name: "Hon. Justice M.A. Ambali FINS, OFR",
      title: "Grand Kadi, Sharia Court of Appeal Ilorin (2000 - 2008)",
      desc: "A product of Az-zumratu Al-dabiyyah Al-Kamaliyyah, Ilorin",
    },
    {
      imgUrl: "./images/Frame 19 (1).png",
      name: "Sheikh Muhyid-deen Hussain",
      title: "Chief Imam of Offa",
      desc: "A product of Az-zumratu Al-dabiyyah Al-Kamaliyyah, Ilorin",
    },
    {
      imgUrl: "./images/Frame 19 (2).png",
      name: "Sheikh Abdur-Raheem Aminullahi Al-Adabyy (d. 2012)",
      desc: "Second National Missioner of Ansarul Islam society of Nigeria",
    },
    {
      imgUrl: "./images/Frame 20.png",
      name: "Hon. Justice I.A. Harron",
      title: "Grand Kadi, Sharia Court of Appeal Ilorin (2000 - 2008)",
      desc:
        "A product of Al-ma'had Al-dabiyyah Owo and Ma'had Ilorin Al-Azhary",
    },
    {
      imgUrl: "./images/Frame 20 (4).png",
      name: "Sheikh Muhammad Basheer salih (OON)",
      title: "The Chief Imam of Ilorin",
      desc: "A product of Ma'had Ilorin Al-Azhary.",
    },
    {
      imgUrl: "./images/Frame 20 (3).png",
      name: "Sheikh Mustapha Ajisafe",
      title:
        "Chief Imam of Osogbo and President, League of Imams and Alfa of Yorubaland",
      desc: "A product of Az-zumratu Al-dabiyyah Al-Kamaliyyah, Ilorin",
    },
    {
      imgUrl: "./images/Frame 20 (2).png",
      name: "Sheikh Imam Abbas bin Muhammad Abubakar",
      title: "The chief Imam of Ikare",
      desc: "A product of Ma'had Ilorin Al-Azhary",
    },
    {
      imgUrl: "./images/Frame 20 (1).png",
      name: "Prof. Abdul-Ganiyy Oladosu (AGAS) OON",
      title: "Chief Imam of University of Ilorin",
      desc: "A product of Az-zumratu Al-dabiyyah Al-Kamaliyyah, Ilorin",
    },
  ];

  return (
    <PageLayout>
      <AboutUsContainer>
        <section className="section-1">
          <div className="hero center">
            <div className="content">
              <Heading h1 lh="4.375rem" mg="0 0 4rem 0" className="head">
                About Mukef
              </Heading>
              <Heading
                color="active"
                fs="1.375rem"
                lh="1.875rem"
                className="sub-head"
              >
                Hi. My name is Khoa. I am a Montreal-based filmmaker, stage
                director and video designer, author of feature films,
                documentaries, essays and video installations. I seek to create
                objects that blur the limits of the sacred, the banal, the real
                and the imaginary. My quest for freedom, my desire to expose the
                artist's creative gestures and impulses pushes me to transcend
                formal boundaries.
              </Heading>
            </div>
            <div className="img">
              <Image imgUrl="./images/image 84.png" />
            </div>
          </div>
        </section>
        <section className="section-2">
          <div className="content center">
            {missionAndVision.map(({ content, head, imgUrl }) => (
              <VisionCard
                key={head}
                head={head}
                content={content}
                imgUrl={imgUrl}
              />
            ))}
          </div>
        </section>
        <section className="section-3">
          <div className="content center">
            <Heading
              color="black"
              h2
              fs="3.24rem"
              lh="3.24rem"
              className="section-3-header"
            >
              Meet our executives
            </Heading>
            <div className="head">
              {executivesHeading.map(({ desc, imgUrl, name }) => (
                <RoundPicCard
                  key={imgUrl}
                  name={name}
                  desc={desc}
                  imgUrl={imgUrl}
                  wt="26.3rem"
                  head
                />
              ))}
            </div>
            <div className="body">
              {executivesBody.map(({ name, desc, title, imgUrl }) => (
                <RoundPicCard
                  key={name}
                  name={name}
                  desc={desc}
                  title={title}
                  imgUrl={imgUrl}
                  wt="17.25rem"
                />
              ))}
            </div>
          </div>
        </section>
      </AboutUsContainer>
    </PageLayout>
  );
};

export default AboutUs;
