import styled from "styled-components"
import { newsData } from "../data/NewsData"
import NewsTemplate from "../molecules/NewsTemplate"
import PageLayout from "../templates/page-template"

const Wrapper = styled.div`
  padding: 12.125rem 0 5.125rem;
  height: 100%;
`
const Top = styled.div`
  margin-bottom: 12rem;
`;
const Bottom = styled.div`
display: grid;
grid-template-columns: 1fr 1fr;
column-gap: 5%;

@media screen and (max-width: 600px){
display: flex;
flex-wrap: wrap;
}
`

const News = () => {
  return (
    <PageLayout>
    <Wrapper>
        <div className="center">
      <Top>
        {newsData.slice(0,2).map(({image}, indx) => <NewsTemplate key={indx} top src={image}/>)}
      </Top>
      <Bottom>
        {newsData.slice(2).map(({image}, indx) => <NewsTemplate key={indx} src={image}/>)}
      </Bottom>
        </div>
    </Wrapper>
    </PageLayout>
  )
}

export default News;