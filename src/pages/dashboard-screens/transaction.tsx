import styled from "styled-components";
import Input from "../../atoms/input";
import { Heading, Text } from "../../atoms/Typography";
import CircularProgressBar from "../../molecules/circular-progress-bar";
import Table from "../../molecules/table";
import TransactionPaymentIndicator from "../../molecules/transaction-payment-indicator";
import TransactionPaymentSmallCard from "../../molecules/transaction-payment-small-card";
import DashboardLayout from "../../templates/dashboard-layout";
import UPDOWNICON from "../../vectors/up-down-icon";

const TransactionContainer = styled.div`
  overflow-x: hidden;
  max-width: 80%;
  margin: 0 auto;
  display: grid;
  row-gap: 4rem;
  .mini-cards {
    display: flex;
    column-gap: 3.625rem;
  }

  .big-card {
    background: #1d231926;
    padding: 1.375rem 2.5rem 2.1rem 2.5rem;
    border-radius: 0.6rem;
    .total-payment-info {
      display: flex;
      justify-content: space-between;
      .year {
        margin-top: 0.5rem;
        display: flex;
        align-items: center;
        gap: 0.5rem;
      }
    }
    .current-total-payment {
      display: flex;
      justify-content: space-between;
      align-items: flex-end;
      .status {
        display: grid;
        grid-template-columns: 1.5fr 1fr;
        width: 75%;
      }
    }
  }
  .table {
    display: flex;
    flex-direction: column;
    row-gap: 2.8rem;
    .search {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
  }
  @media screen and (max-width: 750px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    row-gap: 1rem;
    .mini-cards {
      display: flex;
      flex-direction: column;
      row-gap: 1rem;
    }
    .big-card {
      padding: 1.5rem 1rem;
      border-radius: 0.6rem;
      .total-payment-info,
      .total-payment-year,
      .total-payment-amount,
      .current-total-payment {
        display: flex;
        flex-direction: column;
        align-items: center;
        .total-payment-year {
          row-gap: 0.5rem;
        }

        .year-info,
        .amount-info {
          line-height: 1.7rem;
        }
        .year {
          margin-top: 0rem;
        }
        .total-payment-amount {
          .amount {
            font-size: 3rem;
            line-height: 3.2rem;
          }
        }
      }
      .total-payment-info {
        row-gap: 0.7rem;
      }
      .current-total-payment {
        margin-top: 1rem;
        row-gap: 1rem;
        .status {
          width: 100%;
        }
      }
    }
    .table,
    .search {
      width: 100%;
      display: flex;
      row-gap: 1rem;
      .search {
        flex-direction: column;
        row-gap: 1rem;
        .input {
          width: 70%;
        }
      }
    }
  }
`;

const TransactionPage = () => {
  const totalPayments = [
    {
      color: "#07060D",
      title: "Total payment this year",
      amount: "32,450,000",
    },
    {
      color: "#219653",
      title: "Total payment lifetime",
      amount: "32,450,000",
    },
  ];

  const currentTotalPayments = [
    {
      color: "#189D4D",
      title: "Payment made",
      amount: "32,450,000",
    },
    {
      color: "#999999",
      title: "To complete",
      amount: "32,450,000",
    },
  ];

  const transactionColumns = {
    date: {
      key: "date",
      icon: "toggle",
      label: "date",
    },
    paymentType: {
      key: "paymentType",
      icon: "toggle",
      label: "payment type",
    },
    amount: {
      key: "amount",
      icon: "toggle",
      label: "amount",
    },
    status: {
      key: "status",
      label: "status",
      wt: "5rem",
      align: "center",
    },
  };

  const transactionDatas = [
    {
      date: "Jun 30, 2021",
      paymentType: "Building project support",
      amount: "₦5,000.00",
      status: "success",
    },
    {
      date: "Jun 30, 2021",
      paymentType: "Zakat",
      amount: "₦5,000.00",
      status: "success",
    },
    {
      date: "Jun 30, 2021",
      paymentType: "Orphanage support",
      amount: "₦5,000.00",
      status: "success",
    },
    {
      date: "Jun 30, 2021",
      paymentType: "Water project support",
      amount: "₦5,000.00",
      status: "failed",
    },
  ];

  return (
    <DashboardLayout>
      <TransactionContainer>
        <Heading
          color="black"
          fw={600}
          fs="2rem"
          lh="2.4rem"
          textAlign="center"
        >
          Transaction History
        </Heading>
        <div className="mini-cards">
          {totalPayments.map(({ amount, color, title }) => (
            <TransactionPaymentSmallCard
              color={color}
              amount={amount}
              title={title}
              key={title}
            />
          ))}
        </div>

        <div className="big-card">
          <div className="total-payment-info">
            <div className="total-payment-year">
              <Text
                fw={500}
                fs="1.5rem"
                lh="2.25rem"
                color="black"
                className="year-info"
              >
                Target Payment Information
              </Text>
              <div className="year">
                <Text
                  fs="1.125rem"
                  lh="1.7rem"
                  color="inputBg2"
                  className="year-value"
                >
                  Year 2021
                </Text>
                <UPDOWNICON />
              </div>
            </div>
            <div className="total-payment-amount">
              <Text
                fw={500}
                fs="1.5rem"
                lh="2.25rem"
                color="black"
                textAlign="right"
                mg="0 0 0.31rem 0"
                className="amount-info"
              >
                Target Payment
              </Text>
              <Text
                fw={500}
                fs="3rem"
                lh="4.5rem"
                color="black"
                className="amount"
              >
                ₦1,200,000
              </Text>
            </div>
          </div>
          <div className="current-total-payment">
            <CircularProgressBar value="50%" />
            <div className="status">
              {currentTotalPayments.map(({ amount, color, title }) => (
                <TransactionPaymentIndicator
                  key={title}
                  amount={amount}
                  title={title}
                  color={color}
                />
              ))}
            </div>
          </div>
        </div>

        <div className="table">
          <div className="search">
            <Heading fs="1.25rem" fw={600} lh="1.5rem" color="black">
              Payment History
            </Heading>
            <div className="input">
              <Input prefix placeholder="Search for anything..." />
            </div>
          </div>
          <Table columns={transactionColumns} datas={transactionDatas} />
        </div>
      </TransactionContainer>
    </DashboardLayout>
  );
};

export default TransactionPage;
