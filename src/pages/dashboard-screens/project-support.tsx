import styled from "styled-components";
import ProjectCard from "../../molecules/project-card";
import DashboardLayout from "../../templates/dashboard-layout";

const ProjectSupportContainer = styled.div`
  display: grid;
  row-gap: 2.94rem;
`;

const ProjectSupportPage = () => {
  const recentNews = [
    {
      imgUrl: "../images/Frame 8.png",
      content:
        "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature ",
      name: "Project title goes here",
      id: 0,
    },
    {
      imgUrl: "../images/Frame 9.png",
      content:
        "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature ",
      name: "Project title goes here",
      id: 1,
    },
  ];
  return (
    <DashboardLayout title="Ongoing projects">
      <ProjectSupportContainer>
        {recentNews.map(({ content, id, imgUrl, name }) => (
          <ProjectCard
            key={`project-card-${id}`}
            imgUrl={imgUrl}
            description={content}
            name={name}
          />
        ))}
      </ProjectSupportContainer>
    </DashboardLayout>
  );
};

export default ProjectSupportPage;
