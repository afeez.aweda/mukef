import { useContext } from "react";
import styled from "styled-components";
import Button from "../../atoms/button";
import Title from "../../atoms/title";
import { Text } from "../../atoms/Typography";
import { AppContext } from "../../context/context";
import Table from "../../molecules/table";
import DashboardLayout from "../../templates/dashboard-layout";

const ProfileContainer = styled.div`
  .user-info {
    display: flex;
    flex-direction: column;
    gap: 1.5rem;
    .info {
      display: grid;
      grid-template-columns: 0.5fr 1fr;
      width: 70%;
    }
    .btn {
      align-self: flex-end;
    }
  }
  .table-1 {
    margin-top: 3.6rem;
    margin-bottom: 2.3rem;
    display: flex;
    flex-direction: column;
    row-gap: 2.85rem;
    .btn {
      align-self: flex-end;
    }
  }
  .table-2 {
    display: flex;
    flex-direction: column;
    row-gap: 2.85rem;
    .btn {
      align-self: center;
    }
  }

  @media screen and (max-width: 1300px) {
    .user-info {
      .info {
        grid-template-columns: 0.3fr 1fr;
        width: 100%;
      }
    }
  }

  @media screen and (max-width: 940px) {
    .user-info {
      .info {
        display: flex;
        flex-direction: column;
        row-gap: 0.2rem;
        .title {
          font-size: 0.87rem;
        }
        .value {
          font-size: 1rem;
        }
      }
      .btn {
        align-self: flex-start;
      }
    }
    .table-1 {
      .btn {
        align-self: center;
      }
    }
  }

  @media screen and (max-width: 500px) {
    width: 100%;
    overflow: hidden;
    .user-info {
      .btn {
        align-self: center;
      }
    }
    .table-1 {
      width: 100%;
      overflow-x: auto;
      .btn {
        align-self: center;
      }
    }
  }
`;

const Profile = () => {
  const { dispatch } = useContext(AppContext);
  const userInformation = [
    { title: "Title", value: "Professor" },
    { title: "Full name", value: "Abdul-Ganiyy Oladosu AGAS OON" },
    { title: "Email", value: "profagas@gmail.com" },
    { title: "Phone number", value: "080123456789" },
    { title: "Phone number 2", value: "080123456789" },
    { title: "Home address", value: "No 32 Sheikh Ganiyy Street GRA Ilorin" },
    { title: "State", value: "Kwara" },
    { title: "Nationality", value: "Nigerian" },
    { title: "Favourite quote", value: "إِنَّ سَعْيَكُمْ لَشَتَّىٰ" },
  ];

  const userProjectSupportHistoryColumns = {
    projects: {
      key: "projects",
      label: "projects",
    },
    supportingAmount: {
      key: "supportingAmount",
      label: "Supporting Amount",
    },
    paidAmount: {
      key: "paidAmount",
      label: "Paid Amount",
    },
    remainingAmount: {
      key: "remainingAmount",
      label: "Remaining Amount",
    },
    date: {
      key: "date",
      label: "Date",
      wt: "15%",
    },
    action: {
      key: "action",
      align: "center",
      wt: "5%",
    },
  };

  const userProjectSupportHistory = [
    {
      projects: "water",
      supportingAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "book",
    },
    {
      projects: "orphanage",
      supportingAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "book",
    },
    {
      projects: "building",
      supportingAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "book",
    },
    {
      projects: "zakat",
      supportingAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "book",
    },
  ];
  const userDonationHistoryColumns = {
    year: {
      key: "year",
      label: "year",
    },
    targetAmount: {
      key: "targetAmount",
      label: "target Amount",
    },
    paidAmount: {
      key: "paidAmount",
      label: "paid Amount",
    },
    remainingAmount: {
      key: "remainingAmount",
      label: "remaining Amount",
    },
    date: {
      key: "date",
      label: "date",
      wt: "15%",
    },
    action: {
      key: "action",
      align: "center",
      wt: "5%",
    },
  };
  const userDonationHistory = [
    {
      year: 2021,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2020,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2021,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2020,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2021,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2020,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
    {
      year: 2021,
      targetAmount: "₦ 32,450,000",
      paidAmount: "₦ 450,000",
      remainingAmount: "₦ 32,450,000",
      date: "Jun 1 2022",
      action: "accordion",
    },
  ];

  const handleEditProfile = () => {
    dispatch({ modal: "edit-profile" });
  };

  return (
    <DashboardLayout title="Personal Information">
      <ProfileContainer>
        <div className="user-info">
          {userInformation.map(({ title, value }) => (
            <div className="info" key={title}>
              <Text
                fs="1.02rem"
                lh="1.375rem"
                color="inputBg2"
                className="title"
              >
                {title}
              </Text>
              <Text fs="1.125rem" lh="1.43rem" color="black" className="value">
                {value}
              </Text>
            </div>
          ))}
          <div className="btn">
            <Button title="Edit" onClick={handleEditProfile} />
          </div>
        </div>
        <div className="table-1">
          <Title title="Project support history" />
          <Table
            columns={userProjectSupportHistoryColumns}
            datas={userProjectSupportHistory}
          />
          <div className="btn">
            <Button title="Support a project" />
          </div>
        </div>
        <div className="table-2">
          <Title title="Donation history" />
          <Table
            columns={userDonationHistoryColumns}
            datas={userDonationHistory}
          />
          <div className="btn">
            <Button title="Add donation" />
          </div>
        </div>
      </ProfileContainer>
    </DashboardLayout>
  );
};

export default Profile;
