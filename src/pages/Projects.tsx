import styled from "styled-components"
import { Heading } from "../atoms/Typography"
import { projectsData } from "../data/ProjectsData"
import ProjectsDetails from "../molecules/ProjectsDetails"
import PageLayout from "../templates/page-template"

const Wrapper = styled.div`
  padding-top: 9.25rem;
  @media screen and (max-width: 500px) {
    .heading{
      font-size: 2.5rem;
    }
  }
`
const Projects = () => {
  return (
    <PageLayout>
    <Wrapper>
      <div className="center">
      <Heading className="heading" color="#000" mg="0 0 20px 0" lh="3rem" h2>WE WON’T LIVE WITH POVERTY</Heading>
      </div>
      <ProjectsDetails data={projectsData.slice(0,4)} top text="OUR ONGOING PROJECTS"/>
      <ProjectsDetails data={projectsData.slice(4)} text="OUR COMPLETED PROJECTS"/>
    </Wrapper>
    </PageLayout>
  )
}

export default Projects