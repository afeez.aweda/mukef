import styled from 'styled-components'
import ContactTop from '../molecules/ContactTop'
import ContactCard from '../organisms/ContactCard'
import PageLayout from '../templates/page-template'

const Bottom = styled.div`
  display: flex;
  height: 47rem;
  justify-content: center;
  position: relative;
  @media screen and (max-width: 700px){
    height: 90rem;
}

`
const Contact = () => {
  return (
    <PageLayout>
    <div>
      <ContactTop text1='WANT TO REACH OUT TO US ?' text2='GET IN TOUCH'/>
      <Bottom>
        <ContactCard/>
      </Bottom>
    </div>
    </PageLayout>
  )
}

export default Contact