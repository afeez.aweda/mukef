import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import Button from "../atoms/button";
import Image from "../atoms/image";
import { Heading, Text } from "../atoms/Typography";
import RecentNewsCard from "../molecules/recent-news-card";
import PageLayout from "../templates/page-template";

const HomeContainer = styled.div`
  height: 100%;
  .bg {
    background: ${({ theme }) => theme.colors["secondary"]};
    .hero {
      display: grid;
      grid-template-columns: 1fr 1fr;
      column-gap: 2rem;
      padding: 4.5rem 0 3rem 0;
      .content {
        width: 70%;
      }
    }
  }
  .bg2 {
    background: ${({ theme }) => theme.colors["active"]};
    .recent-news {
      padding: 6.75rem 0;
      display: flex;
      flex-direction: column;
      .btn {
        align-self: center;
      }
    }
  }
  @media screen and (max-width: 1389px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media screen and (max-width: 1110px) {
    .bg {
      .hero {
        display: flex;
        flex-direction: column;
        text-align: center;
        max-width: 85%;
        margin: 0 auto;
        .content {
          width: 100%;
        }
        .img {
          display: none;
        }
      }
    }
  }
  @media screen and (max-width: 500px) {
    .bg {
      .hero {
        .content {
          .home-title {
            font-size: 2.8rem;
            line-height: 3.2rem;
          }
        }
      }
    }
  }
  @media screen and (max-width: 400px) {
    .bg {
      .hero {
        .content {
          .home-title {
            font-size: 2.4rem;
          }
        }
      }
    }
  }
`;

const Home = () => {
  const navigate = useNavigate();
  const handleNavigate = () => {
    navigate("/subscribe");
  };
  const recentNews = [
    {
      imgUrl: "./images/Frame 8.png",
      content: "The best cities for traveling this summer",
    },
    {
      imgUrl: "./images/Frame 9.png",
      content: "The best cities for traveling this summer",
    },
  ];
  return (
    <PageLayout>
      <HomeContainer>
        <div className="bg">
          <div className="hero center">
            <div className="content">
              <Heading h1 lh="4.3rem" className="home-title">
                Muhammad Kamalud-Deen (Education) Foundation
              </Heading>
              <Text
                fs="1.375rem"
                mg="1.3rem 0 2.8rem 0"
                lh="2rem"
                color="light2"
              >
                The most elegant expression of Apple Watch returns with two
                iconic materials — titanium and ceramic.{" "}
              </Text>
              <Button title="Subscribe" onClick={handleNavigate} />
            </div>
            <div className="img">
              <Image imgUrl="./images/mosque.png" alt="" />
            </div>
          </div>
        </div>
        <div className="bg2">
          <div className="recent-news center">
            <Heading
              className="news-head"
              color="secondary"
              fw={700}
              mg="0 0 2.5rem 0"
              fs="1.5rem"
            >
              Recent news
            </Heading>
            {recentNews.map(({ imgUrl, content }, i) => (
              <RecentNewsCard imgUrl={imgUrl} content={content} key={i} />
            ))}
            <div className="btn">
              <Button title="More news" border outline color="primary" />
            </div>
          </div>
        </div>
      </HomeContainer>
    </PageLayout>
  );
};

export default Home;
