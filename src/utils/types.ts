import {
  ChangeEvent,
  ChangeEventHandler,
  DetailedHTMLProps,
  ReactNode,
} from "react";

export interface ButtonProps {
  title?: string;
  onClick?: () => void;
  type?: "button" | "reset" | "submit";
  outline?: boolean;
  mg?: string;
  pd?: string;
  hg?: string;
  wt?: string;
  color?: string;
  border?: boolean;
}

export type InputProps = {
  width?: string;
  title?: string;
  value?: string;
  description?: string;
  required?: boolean;
  name?: string;
  type?: string;
  maxLength?: number;
  error?: string | null;
  handleChange?:
    | ChangeEventHandler<HTMLInputElement>
    | ChangeEventHandler<HTMLSelectElement>;
  placeholder?: string;
  show?: boolean;
} & DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

export interface TextProps {
  fw?: number;
  fs?: string;
  width?: string;
  h1?: boolean;
  h2?: boolean;
  h4?: boolean;
  bold?: boolean;
  color?: string;
  lh?: string;
  pd?: string;
  mg?: string;
  decoration?: string;
  hoverColor?: string;
  height?: string;
  textAlign?: string;
}

export interface ContainerProps {
  display?: string;
  column?: string;
  justify?: string;
  items?: string;
  mg?: string;
  pd?: string;
  wt?: string;
  ht?: string;
  bg?: string;
  children?: ReactNode;
  center?: boolean;
}

export interface ImgProps {
  wt?: string;
  round?: boolean;
  ht?: string;
  imgUrl: string;
  alt?: string;
}

export type eventProp =
  | ChangeEvent<HTMLInputElement>
  | ChangeEvent<HTMLSelectElement>;

export type dataType = Record<string, string>;

export interface ModalProps {
  isActive?: boolean;
  wt?: string;
  ht?: boolean;
  children: ReactNode;
}

interface MiniCardProps {
  color?: string;
  title: string;
  amount: string;
}

export type objectDatasType = Record<
  string,
  string | number | Record<string, string>
>;
