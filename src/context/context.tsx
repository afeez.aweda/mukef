import React, { createContext, ReactNode, useReducer } from "react";
import ModalReducer, { ActionType } from "./reducer";

export type ContextType = {
  state: {
    isModal: boolean;
    modalType?: null | string;
  };
  dispatch: React.Dispatch<ActionType>;
};

const INITIAL_STATE = {
  isModal: false,
  modalType: null,
};

export const AppContext = createContext<ContextType>({
  state: INITIAL_STATE,
  dispatch: () => null,
});

export const AppContextProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(ModalReducer, INITIAL_STATE);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};
