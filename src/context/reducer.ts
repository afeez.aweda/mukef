export type ActionType = {
  type?: "TOGGLE" | "TRUE" | "FALSE";
  modal?: null | string;
};

const ModalReducer = (
  state: {
    isModal: boolean;
    modalType?: null | string;
  },
  action: ActionType
) => {
  const { type, modal } = action;
  if (modal === "login") {
    return {
      isModal: true,
      modalType: modal,
    };
  }
  if (modal === "edit-profile") {
    return {
      isModal: true,
      modalType: modal,
    };
  }if (modal === "support-project") {
    return {
      isModal: true,
      modalType: modal,
    };
  } else if (type === "FALSE") {
    return {
      isModal: false,
      modalType: state.modalType,
    };
  } else if (type === "TRUE") {
    return {
      isModal: true,
      modalType: state.modalType,
    };
  } else if (type === "TOGGLE") {
    return {
      isModal: !state.isModal,
      modalType: state.modalType,
    };
  } else {
    return state;
  }
};

export default ModalReducer;
