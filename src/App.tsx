import { Route, Routes } from "react-router-dom";
import AboutUs from "./pages/about-us";
import Contact from "./pages/Contact";
import Profile from "./pages/dashboard-screens/profile";
import ProjectSupportPage from "./pages/dashboard-screens/project-support";
import TransactionPage from "./pages/dashboard-screens/transaction";
import Home from "./pages/home";
import News from "./pages/News";
import Projects from "./pages/Projects";
import Subscribe from "./pages/Subscribe";

function App() {
  return (
    <Routes>
      <Route path="dashboard">
        <Route path="profile" element={<Profile />} />
        <Route path="support" element={<ProjectSupportPage />} />
        <Route path="transactions" element={<TransactionPage />} />
      </Route>
      <Route path="contact" element={<Contact />} />
      <Route path="subscribe" element={<Subscribe />} />
      <Route path="news" element={<News />} />
      <Route path="projects" element={<Projects />} />
      <Route path="about-us" element={<AboutUs />} />
      <Route path="/" element={<Home />} />
    </Routes>
  );
}

export default App;
